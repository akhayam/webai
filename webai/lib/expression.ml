module SMap = Map.Make(String)

(* lang_expr functor type *)
module type FT_lang_expr = functor (VFT : Value.FT_lang_values) (FTB : Base_types.FT_base_types) (CT : Concrete_types.concrete_types) -> sig
   open VFT(FTB)(CT)

   type ('a,'b) standard_e =
   | Value of 'b ext_v
   | Var of var
   | Binary of (binary_operator * ('a,'b) ext_e * ('a,'b) ext_e)
   | Unary of (unary_operator * ('a,'b) ext_e)
   and ('a,'b) ext_e =
   | StdExpr of ('a,'b)  standard_e
   | ExtExpr of 'a
   and var = String.t
   and binary_operator =
   | Add
   | Sub
   | Mul
   | LEq
   | L
   | GEq
   | G
   | Eq
   | And
   | Or
   and unary_operator =
   | Neg
   val eval_expression : ('a,'b) ext_e -> ('b ext_v) SMap.t -> 'b ext_v
end

(* lang_expr functor *)

module F_lang_expr = functor (VFT : Value.FT_lang_values) (FTB : Base_types.FT_base_types) (CT : Concrete_types.concrete_types) -> struct
   include CT
   include FTB(CT)
   include VFT(FTB)(CT)

   type ('a,'b) standard_e =
   | Value of 'b ext_v
   | Var of var
   | Binary of (binary_operator * ('a,'b) ext_e * ('a,'b) ext_e)
   | Unary of (unary_operator * ('a,'b) ext_e)
   and ('a,'b) ext_e =
   | StdExpr of ('a,'b)  standard_e
   | ExtExpr of 'a
   and var = String.t
   and binary_operator =
   | Add
   | Sub
   | Mul
   | LEq
   | L
   | GEq
   | G
   | Eq
   | And
   | Or
   and unary_operator =
   | Neg
   let rec eval_expression (e : ('a,'b) ext_e) (m : ('b ext_v) SMap.t): 'b ext_v =
      (match e with
      | StdExpr se ->
         (match se with
         | Value v -> v
         | Var x ->
            (match SMap.find_opt x m with | Some v -> v | None -> failwith ("variable "^x^" does not have a binding"))
         | Binary (o,e1,e2) ->
            (match o with
            | Add -> (match (eval_expression e1 m, eval_expression e2 m) with
                     | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Nat (_add n1 n2))
                     | _ -> failwith "+ takes only nat parameters")
            | Sub -> (match (eval_expression e1 m, eval_expression e2 m) with
                     | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Nat (_sub n1 n2))
                     | _ -> failwith "- takes only nat parameters")
            | Mul -> (match (eval_expression e1 m, eval_expression e2 m) with
                     | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Nat (_mul n1 n2))
                     | _ -> failwith "* takes only nat parameters")
            | L -> (match (eval_expression e1 m, eval_expression e2 m) with
                     | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Bool (_l n1 n2))
                     | _ -> failwith "< takes only nat parameters")
            | LEq -> (match (eval_expression e1 m, eval_expression e2 m) with
                     | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Bool (_le n1 n2))
                     | _ -> failwith "<= takes only nat parameters")
            | G -> (match (eval_expression e1 m, eval_expression e2 m) with
                     | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Bool (_g n1 n2))
                     | _ -> failwith "> takes only nat parameters")
            | GEq -> (match (eval_expression e1 m, eval_expression e2 m) with
                     | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Bool (_ge n1 n2))
                     | _ -> failwith ">= takes only nat parameters")
            | Eq -> (match (eval_expression e1 m, eval_expression e2 m) with
                     | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Bool (_ieq n1 n2))
                     | (StdVal (Bool b1), StdVal (Bool b2)) -> StdVal (Bool (_beq b1 b2))
                     | (StdVal (Str s1), StdVal (Str s2)) -> StdVal (Bool (_seq s1 s2))
                     | (StdVal Unit, StdVal Unit) -> StdVal (Bool _true)
                     | (StdVal Undefined, StdVal Undefined) -> StdVal (Bool _false)
                     | _ -> failwith "= does not allow mixed expression")
            | And -> (match (eval_expression e1 m, eval_expression e2 m) with
                     | (StdVal (Bool b1), StdVal (Bool b2)) -> StdVal (Bool (_and b1 b2))
                     | _ -> failwith "&& takes only bool parameters")
            | Or -> (match (eval_expression e1 m, eval_expression e2 m) with
                     | (StdVal (Bool b1), StdVal (Bool b2)) -> StdVal (Bool (_or b1 b2))
                     | _ -> failwith "|| takes only bool parameters"))
         | Unary (o,e1) -> match o with
                           | Neg ->
                              match eval_expression e1 m with
                              | StdVal (Nat n) -> StdVal (Nat (_mul n (mk_nat (-1))))
                              | StdVal (Bool b) -> if is_true (_beq b _true) then StdVal (Bool (_false)) else StdVal (Bool (_true))
                              | _ -> failwith "! takes only bool and nat")
      | ExtExpr _ -> StdVal Undefined) (* to define as soon as I put extensions of expressions as parameter of the Functor*)                    
end

(* Expressions instantiation *)
module E_inst = F_lang_expr(Value.F_lang_values)(Base_types.F_base_types)(Concrete_types.CONCRETE_TYPES)