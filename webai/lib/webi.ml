module S = Server.SERVER_IMP_SYM
module C = Client.CLIENT_IMP_SYM
module D = Device.D_SYM

module Webi_SYM = struct
  include S
  include C
  include D

  type lang =
  | Lst of (lang * lang)
  | SRV of (url * var * hostName * server_language) (* service URL : hostA (x) := ... *)
  | CL of (url * (client_id * client_value)) (* client 1 calls URL with param VALUE *)
  | Dev of (device_id * ((actuator * device_values) List.t) Option.t * D.device_values Option.t) (* device (window, kitchen) default win_status = false and actuators -> [(open, true);(close, false)] *)

  type world_oracle = ((device_id * actuator) * (device_id * device_values)) List.t (* relations between actuator and sensors *)

end

