module CT = Base_types.BT_SYM
module C = Client.CLIENT_IMP_SYM
module S = Server.SERVER_IMP_SYM
module D = Device.D_SYM
module W = Webi.Webi_SYM
module Z = Concrete_types.Z3_TYPES

module SSMap = Map.Make(
  struct
    type t = (D.device_name * D.actuator)
    let compare (n1,a1) (n2,a2) =
        let c = String.compare n1 n2 in
        match c = 0 with
        | true -> String.compare a1 a2
        | false -> c
  end)

  (* map used to store graph nodes. names are built in the following way:
     1) device ID is a pair of strings (name, location)
     2) actuator means the nome of the Actuator, and it is optional.
     3) if actuator is some, the name of the device is the following pair => ((name ^ "_" ^ location), actuator)
     4) if actuator is none, then it must be a sensot. the node name is the following pair : ((name ^ "_" ^ location), "__SENSOR__")
   *)
  module SSVMap = Map.Make(
    struct
    type t = (D.device_name * D.actuator * S.server_value)
    let compare (n1,a1,v1) (n2,a2,v2) =
        let c = String.compare n1 n2 in
        match c = 0 with
        | true -> (match String.compare a1 a2 with
                  | 0 -> 0
                  | _ -> S.value_compare v1 v2)
        | false -> c
  end

  )

module SMap = Map.Make(String)

module type analyzer = sig

  type dwo = (D.device_id * D.device_id)
  type source = D.url List.t
  type sink = (D.device_name * D.actuator)
  type map_actuators_values = D.device_values SSMap.t
  type taint = | Taint | Untaint

  (* dependency graph stuff *)
  type t_node =
  | Actuator of (D.device_id * D.actuator * S.server_value)
  | Sensor of (D.device_id * S.server_value)
  | Conditional_Relation of S.server_expr
  (*| WO_Relation of ((D.device_id * D.actuator) *  (D.device_id * D.device_values))*)

  type dependency_graph

  type dep_vertex

  type nodeMap = dep_vertex SSMap.t

  type st_service = S.server_value SMap.t
  type st_rel_var_dev = (D.device_id List.t) SMap.t

  type settings = {
    world_oracle : W.world_oracle;
    dwo : dwo;
    source : source;
    sink : sink;
    map_act_val : map_actuators_values;
  }

  type preprocessing_webi = {
    devices : (D.device_id * ((D.actuator * D.device_values) List.t) Option.t * D.device_values Option.t) List.t;
    services : (D.url * S.var * S.hostName * S.server_language) List.t;
    clients : (D.url * (C.client_id * C.client_value)) List.t;
  }

  val preprocess_webi : W.lang -> preprocessing_webi
  val analyzer : settings -> dependency_graph -> W.lang ->  dependency_graph

end

module ANALYZER : analyzer = struct
  include Z
  include Graph
  

  type dwo = (D.device_id * D.device_id)
  type source = D.url List.t
  type sink = (D.device_name * D.actuator)
  type map_actuators_values = D.device_values SSMap.t

  type taint = | Taint | Untaint

  (* dependency graph stuff *)
  type t_node =
  | Actuator of (D.device_id * D.actuator * S.server_value)
  | Sensor of (D.device_id * S.server_value)
  | Conditional_Relation of S.server_expr
  (*| WO_Relation of ((D.device_id * D.actuator) *  (D.device_id * D.device_values)) *)(* error... here II should draw an arrow between actuator and sensor *)

  module Dependency_graph = Graph.Imperative.Graph.Abstract(struct type t = t_node end)

  type dependency_graph = Dependency_graph.t

  type dep_vertex = Dependency_graph.vertex

  type nodeMap = dep_vertex SSMap.t

  type st_service = S.server_value SMap.t
  type st_rel_var_dev = (D.device_id List.t) SMap.t

  type settings = {
    world_oracle : W.world_oracle;
    dwo : dwo;
    source : source;
    sink : sink;
    map_act_val : map_actuators_values;
  }

  type preprocessing_webi = {
    devices : (D.device_id * ((D.actuator * D.device_values) List.t) Option.t * D.device_values Option.t) List.t;
    services : (D.url * S.var * S.hostName * S.server_language) List.t;
    clients : (D.url * (C.client_id * C.client_value)) List.t;
  }

  let preprocess_webi p =
      let rec aux_preprocess_webi p pre_w = 
      (match p with
      | W.SRV p' -> {pre_w with services = pre_w.services@[p']}
      | W.CL p' -> {pre_w with clients = pre_w.clients@[p']}
      | W.Dev p' -> {pre_w with devices = pre_w.devices@[p']}
      | W.Lst (p1,p2) -> let pre_w' = aux_preprocess_webi p1 pre_w in aux_preprocess_webi p2 pre_w') in
                         aux_preprocess_webi p {devices = []; services = []; clients = []}

  let rec analyzer (s : settings) (g : dependency_graph) (p : W.lang) =
      let node_map = SSVMap.empty in
      let r_p = preprocess_webi p in
      let (g,node_map') = graph_of_world_oracle g node_map s.world_oracle s.map_act_val in
      let rec aux_analyze_devices l_d node_map = (
        match l_d with
        | [] -> node_map
        | x :: t -> (
            match x with 
            | (id,lact,vsens) -> let node_map' = analyze_device g node_map (id,lact,vsens) in aux_analyze_devices t node_map')
      ) in
      let node_map'' = aux_analyze_devices r_p.devices node_map' in
      let rec aux_analyze_services l_s node_map =(
        let state = SMap.empty in
        let rel_map = SMap.empty in
        match l_s with
        | [] -> ()
        | x :: t -> (
          match x with
          | (_,_,_,p') -> let _ = analyze_server s g p' state rel_map node_map None in (* updating g *)
                          aux_analyze_services t node_map (* We are supposed to have all the nodes related to actuators and sensors already in node_map *)
        )
      ) in aux_analyze_services r_p.services node_map''; g
      

  (* WORLD_ORACLE subgraph *)
  (* For each relation defined by the world oracle, we build that part of graph and store mappings from node names to nodes into the node_map.
     What we build here is a forest of relations between devices. *)
  and graph_of_world_oracle g node_map wo map_act_val =
        match wo with
        | [] -> (g,node_map)
        | h :: t -> match h with
                    | ((ida,a),(ids,v)) ->
                            let v_act = Dependency_graph.V.create
                              (Actuator (ida,
                                         a,
                                         match SSMap.find_opt ida map_act_val with
                                         | Some v -> dev_to_srv_value v
                                         | None -> failwith "actuator value not in the map.
                                                             graph_of_world_oracle")) in
                            let v_sens = Dependency_graph.V.create 
                              (Sensor (ids,dev_to_srv_value v)) in
                            Dependency_graph.add_vertex g v_act;
                            Dependency_graph.add_vertex g v_sens;
                            Dependency_graph.add_edge g v_act v_sens;
                            let node_map' = SSVMap.add
                               (match ida with (n,l) -> (n ^ "_" ^ l),
                                a,
                                find_device_act_val map_act_val ida a)
                               v_act
                               node_map in
                            let node_map'' = SSVMap.add 
                              (match ids with (n,l) -> (n ^ "_" ^ l),
                               "__SENSOR__",
                               dev_to_srv_value v) 
                              v_sens
                              node_map' in  (* too weak *)
                            graph_of_world_oracle g node_map'' t map_act_val
  and dev_to_srv_value (vd : D.device_values) =
      match vd with
      | D.StdVal v -> S.StdVal v
      | D.ExtVal _ -> S.StdVal Undefined
  and find_device_act_val (m : map_actuators_values) ((n,_) : D.device_id) (a : D.actuator) : S.server_value =
      match SSMap.find_opt (n,a) m with
      | Some v -> dev_to_srv_value v
      | None -> failwith ("device "^n^" have no value in the map")
  

  (* DEVICE DECLARATION subgraph *)
  (* We will add all the device nodes to the graph. There will be no edges at all.
     Only vertices. In case some vertices have been already defined by the WORLD ORACLE then do nothing.
     Otherwise add to the graph.*)
  and analyze_device g node_map (id,lact,vsens) =
      match lact with
      | None -> let v = Dependency_graph.V.create (
                          Sensor (id,
                                  match vsens with
                                  | Some vsens' -> dev_to_srv_value vsens'
                                  | _ -> failwith "Not possible. sensors have values")) in
                        Dependency_graph.add_vertex g v;
                        SSVMap.add
                          (match id with (n,l) -> (n ^ "_" ^ l), "__SENSOR__",
                           dev_to_srv_value (match vsens with
                                             | Some v -> v
                                             | _ -> failwith "not possible"))
                          v
                          node_map
      | Some l -> let rec aux_analyze_device l node_map = (
                    match l with
                    | [] -> node_map
                    | (a,v) :: t ->
                      let v_act = Dependency_graph.V.create
                        (Actuator (id,a,dev_to_srv_value v)) in
                      Dependency_graph.add_vertex g v_act;
                      let node_map' = SSVMap.add
                        (match id with (n,l) -> (n ^ "_" ^ l),
                         a,
                         dev_to_srv_value v)
                        v_act
                        node_map in
                      aux_analyze_device t node_map') in
                  aux_analyze_device l node_map;


  (* SERVICES DECLARATION subgraph*)
  and analyze_server (s : settings) (g : dependency_graph) (p : S.server_language) (m : st_service) (r : st_rel_var_dev) (node_map : dep_vertex SSVMap.t) (v_context : dep_vertex Option.t): (st_service * st_rel_var_dev *dep_vertex SSVMap.t * dep_vertex Option.t) List.t =
      (match p with
      | StdLan p' ->
          (match p' with
          | S.Assignment (x,e) ->
              let keys_r = List.fold_left (fun l t -> match t with | (k,_) -> [k]@l) [] (SMap.bindings r) in (* getting all the variables that have relations with devices*)
              let keys_e = keys_in_expr keys_r e in (*keeping only the variables that appear in the expression e*)
              let var_devices_relations = get_variable_relations keys_e r in (* get the devices related to the variable x*)
              let r' = SMap.add x var_devices_relations r in (* updating the var -> deviceID relation map*)
              let v = S.eval_expression e m in (* evaluating the expression*)
              let m' = SMap.add x v m in (* updating the value binding of x in m *)
              [(m',r',node_map,v_context)]
          | S.Sequence (p1,p2) ->
              let l_res = analyze_server s g p1 m r node_map v_context in
              List.flatten (List.map (fun (m,r,nm,v_cxt) -> analyze_server s g p2 m r nm v_cxt) l_res) (* flattening to not obtain list of lists *)
          | S.Skip -> [(m,r,node_map,v_context)]
          | S.Return _ -> [(m,r,node_map,v_context)]
          | S.If (e,pt,pf) ->
              let keys_r = List.fold_left (fun l t -> match t with | (k,_) -> [k]@l) [] (SMap.bindings r) in (* getting all the variables that have relations with devices*)
              let keys_e = keys_in_expr keys_r e in (*  keys_r is composed by all the variables in the expression that have relations with devices*)
              let vs_e = keys_vertices s keys_e r in
              let v_e = Dependency_graph.V.create (Conditional_Relation e) in
              let v_ne = Dependency_graph.V.create (Conditional_Relation (W.StdExpr (Unary (Neg, e)))) in
              (* don't really know how to name them *)
              Dependency_graph.add_vertex g v_e;
              Dependency_graph.add_vertex g v_ne;
              (* in case of dependencies to devices, I need to add actuators that might satisfy the expression e.
               Actuators that do not satisfy e connects with !e.
               If there is a relation to a sensor, then choose again the node that have sensor values that satisfies e. *)

              (* add expression to the map name nodes*)
              let rec aux_g l g ve =
                match l with
                | [] -> (match v_context with
                        | None -> ()
                        | Some v_context -> Dependency_graph.add_edge g v_context ve)
                | v :: t -> Dependency_graph.add_edge g v ve; aux_g t g ve
              in
              aux_g vs_e g v_e;
              aux_g vs_e g v_ne;
              let l_res' =  analyze_server s g pt m r node_map (Some v_e) in (* TOFIX : pt builds a subgraph from v_e*)
              let l_res'' = analyze_server s g pf m r node_map (Some v_ne) in (* TOFIX : pf builds a subgraph from v_ne*)
              (l_res')@(l_res'')
              (* analyze p1 obtaining (m',r') from (m,r)*)
              (* analyze p2 obtaining (m'',r'') from (m,r)*)
              (* to discuss how to combine (m',r') (m'',r'') *)
              (* update node_map*)
          | S.While _ -> [(m,r,node_map,v_context)]
          )
      (*| ExtLan p' -> (
          match p' with
          | Act (a,id,_) -> (let v_act = SSVMap.find_opt (match id with (n,l) -> (n ^ "_" ^ l),a,find_device_act_val s.map_act_val id a) node_map in
                            match v_act with
                            | None -> failwith "all devices are declared in preprocessing. Impossible to not finding it."
                            | Some v_act -> match v_context with | None -> )
          | Get _ -> []) *)
      | _ -> failwith "work in progress")
  and keys_in_expr (keys : S.var List.t) (e : S.server_expr) : S.var List.t =
      let vars_e = vars_in_expr e [] in
      List.filter (fun x -> List.mem x vars_e) keys
  and vars_in_expr (e : S.server_expr) (l : S.var List.t): S.var List.t =
      match e with
      | ExtExpr _ -> l
      | StdExpr se ->
        (match se with
        | Value _ -> l
        | Var x -> [x]@l
        | Binary (_,e1,e2) -> List.rev (List.fold_left cons_uniq [] ((vars_in_expr e1 l)@(vars_in_expr e2 l))) 
        | Unary (_,e') -> List.rev (List.fold_left cons_uniq [] (vars_in_expr e' l)))
  and cons_uniq xs x = if List.mem x xs then xs else x :: xs
  and keys_vertices (_ : settings) (_ : S.var List.t) (_ : st_rel_var_dev) : dep_vertex List.t = [] (* take all the vertices related to vars in keys.*)
  and get_variable_relations l r =
      match l with
      | [] -> []
      | h :: t -> 
        (match (SMap.find_opt h r) with
        | None -> failwith "impossible to not have a relation to a device here"
        | Some id -> id@(get_variable_relations t r))
end