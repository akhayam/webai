module type FT_lang_device = functor (FTV : Value.FT_lang_values) (FTB : Base_types.FT_base_types) (CT : Concrete_types.concrete_types) -> sig

  type url
  type permission
  type device_name
  type device_location
  type device_id
  type actuator

  type device_values = FTV(FTB)(CT).not_implemented FTV(FTB)(CT).ext_v
end

module F_lang_device = functor (FTV : Value.FT_lang_values) (FTB : Base_types.FT_base_types) (CT : Concrete_types.concrete_types) -> struct
  include CT
  include FTB(CT)
  include FTV(FTB)(CT)

  type url = string
  type permission = unit
  type device_name = string
  type device_location = string
  type device_id = (device_name * device_location)
  type actuator = string

  type device_values = not_implemented ext_v
end

module D_SYM = F_lang_device(Value.F_lang_values)(Base_types.F_base_types)(Concrete_types.Z3_TYPES)