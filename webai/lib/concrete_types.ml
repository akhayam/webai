module Lst = List
module SetO = Set

module type concrete_types = sig
  type _cbool
  type _cint
  type _cstring

  val _ctrue : _cbool
  val _cfalse : _cbool

  val _cisTrue : _cbool -> bool
  val mk_nat : int -> _cint

  val _cadd : _cint -> _cint -> _cint
  val _csub : _cint -> _cint -> _cint
  val _cmul : _cint -> _cint -> _cint

  val _cand : _cbool -> _cbool -> _cbool
  val _cor : _cbool -> _cbool -> _cbool

  val _cl : _cint -> _cint -> _cbool
  val _cg : _cint -> _cint -> _cbool
  val _cle : _cint -> _cint -> _cbool
  val _cge : _cint -> _cint -> _cbool
  val _cieq : _cint -> _cint -> _cbool
  val _cbeq : _cbool -> _cbool -> _cbool
  val _cseq : _cstring -> _cstring -> _cbool
  val _cbool_compare : _cbool -> _cbool -> int
  val _cint_compare : _cint -> _cint -> int
  val _cstring_compare : _cstring -> _cstring -> int

  (* missing type constructor functions + mappings between Z3 and custom symbolic expressions *)
end

module CONCRETE_TYPES = struct
  type _cbool = bool
  type _cint = int
  type _cstring = string
  type _cvar = string


  let _ctrue = true
  let _cfalse = false
  let _cisTrue (b : _cbool) : bool = b
  let mk_nat n = n

  let _cadd n1 n2 = n1 + n2
  let _csub n1 n2 = n1 - n2
  let _cmul n1 n2 = n1 * n2

  let _cand b1 b2 = b1 && b2
  let _cor b1 b2 = b1 || b2

  let _cl (n1 : _cint) (n2 : _cint) = n1 < n2
  let _cg (n1 : _cint) (n2 : _cint) = n1 > n2
  let _cle (n1 : _cint) (n2 : _cint) = n1 <= n2
  let _cge (n1 : _cint) (n2 : _cint) =  n1 >= n2
  let _cieq (n1 : _cint) (n2 : _cint) = n1 == n2
  let _cbeq (b1 : _cbool) (b2 : _cbool) = b1 == b2
  let _cseq (s1 : _cstring) (s2 : _cstring) = s1 == s2
  let _cbool_compare (b1 : _cbool) (b2: _cbool) = Bool.compare b1 b2
  let _cint_compare (i1 : _cint) (i2 : _cint) = Int.compare i1 i2
  let _cstring_compare (s1 : _cstring) (s2 : _cstring) = String.compare s1 s2
end

module SSet = SetO.Make(String)
let used_names = ref SSet.empty
let clear_names : unit =
  used_names := SSet.empty
let new_name (vname: string) =
  let rec aux (i: int) : string =
    begin
      let str0 = String.concat "" [vname; "_"; Int.to_string i] in
      let op0 = (try Some (SSet.find str0 !used_names) with Not_found -> None) in
      if (op0 = None ) then
        begin
          used_names := SSet.add str0 !used_names;
          str0
        end
      else
        aux (i+1)
    end in
  aux 0

let print_expr chan e = Printf.fprintf chan "%s" (Z3.Expr.to_string e)
let rec print_prop chan xs : unit =
  match xs with
  | [] -> ()
  | [x] -> print_expr chan x
  | x::y::xs ->
     Printf.fprintf chan "%a /\\ " print_expr x;
     print_prop chan (y::xs)


module Analyzer_interface = struct
  include Z3

  type zexpr = Z3.Expr.expr
  type solver_context = Z3.context
  type zsolver = Z3.Solver.solver
  type zsymbol = Z3.Symbol.symbol
  type sym_path = zexpr List.t
  type zstatus = | SAT | UNSAT | UNKNOWN

  let main_config : (string * string) list = [("model", "true"); ("proof", "false")]
  let main_context: solver_context = Z3.mk_context main_config
  let isort = (Z3.Arithmetic.Integer.mk_sort main_context)
  let rec check (zls : zexpr list) =
    let g = Z3.Goal.mk_goal main_context true false false in
    Z3.Goal.add g zls;
    let solver = Z3.Solver.mk_solver main_context None in
    Lst.iter (fun a -> (Z3.Solver.add solver [ a ])) (Z3.Goal.get_formulas g);
    let status = (z3status_to_zstatus (Z3.Solver.check solver [])) in
    (status, solver)
  and z3status_to_zstatus z3stat =
    match z3stat with
    | Z3.Solver.SATISFIABLE -> SAT
    | Z3.Solver.UNSATISFIABLE -> UNSAT
    | Z3.Solver.UNKNOWN -> UNKNOWN
  let mk_true = Z3.Boolean.mk_true main_context
  let mk_false = Z3.Boolean.mk_false main_context
  let mk_add l = Z3.Arithmetic.mk_add main_context l
  let mk_and l = Z3.Boolean.mk_and main_context l
  let mk_bool e =
       match e with
       | true -> Z3.Boolean.mk_val main_context true
       | false -> Z3.Boolean.mk_val main_context false
  let mk_const e =  (Z3.Expr.mk_const main_context e isort)
  let mk_div n1 n2 = (Z3.Arithmetic.mk_div main_context n1 n2)
  let mk_eq (e1, e2) = (Z3.Boolean.mk_eq main_context e1 e2)
  let mk_ge (n1, n2) = (Z3.Arithmetic.mk_ge main_context n1 n2)
  let mk_g (n1,n2) = (Z3.Arithmetic.mk_gt main_context n1 n2)
  let mk_le (n1, n2) = (Z3.Arithmetic.mk_le main_context n1 n2)
  let mk_l (n1,n2) = (Z3.Arithmetic.mk_lt main_context n1 n2)
  let mk_mul l = (Z3.Arithmetic.mk_mul main_context l)
  let mk_not e = (Z3.Boolean.mk_not main_context e)
  let mk_numeral_i n = (Z3.Arithmetic.Integer.mk_numeral_i main_context n)
  let mk_or l = (Z3.Boolean.mk_or main_context l)
  let mk_string e = (Z3.Symbol.mk_string main_context e)
  let mk_sub l = (Z3.Arithmetic.mk_sub main_context l)
  let to_string s = (Z3.Expr.to_string s)
  let fresh_sym s = (mk_const (Z3.Symbol.mk_string main_context (new_name s)))

  let get_string_model zls (zstat, zsolv) =
    if Lst.length zls > 0 && zstat = SAT then
      let m = Z3.Solver.get_model zsolv in
      match m with
      | Some m -> Some (Z3.Model.to_string m)
      | None -> None
    else
      None
  let get_model (sp: sym_path) : unit =
    let (status, solver) = check sp in
    match status with
      | SAT ->
        begin
          Printf.printf "Assertion violated,";
          match get_string_model sp (status, solver) with
          | Some m -> Printf.printf "model:\n%s\n" m
          | None -> Printf.printf "failed to get model.\n"
        end;
      | _ -> Printf.printf  "Assertion verified.\n"
end


module Z3_TYPES = struct
  include Analyzer_interface

  
  type _cbool = zexpr
  type _cint = zexpr
  type _cstring = string

  let _ctrue = mk_bool true
  let _cfalse = mk_bool false
  let _cisTrue (b : _cbool) : bool = match Z3.Expr.compare _ctrue b with | 0 -> false | _ -> true
  let mk_nat n = mk_numeral_i n
  
  let _cadd n1 n2 = mk_add [n1;n2]
  let _csub n1 n2 = mk_sub [n1;n2]
  let _cmul n1 n2 = mk_mul [n1;n2]
      
  let _cand b1 b2 = mk_and [b1;b2]
  let _cor b1 b2 = mk_or [b1;b2]
      
  let _cl (n1 : _cint) (n2 : _cint) = mk_l (n1,n2)
  let _cg (n1 : _cint) (n2 : _cint) = mk_g (n1,n2)
  let _cle (n1 : _cint) (n2 : _cint) = mk_le (n1, n2)
  let _cge (n1 : _cint) (n2 : _cint) = mk_ge (n1, n2)
  let _cieq n1 n2 = mk_eq (n1, n2)
  let _cbeq b1 b2 = mk_eq (b1,b2)
  let _cseq (s1 : _cstring) (s2 : _cstring) = match s1 == s2 with | true -> mk_true | false -> mk_false (* blasphemy done to see type check *)
  let _cbool_compare (b1 : _cbool) (b2: _cbool) = Z3.Expr.compare b1 b2
  let _cint_compare (i1 : _cint) (i2 : _cint) = Z3.Expr.compare i1 i2
  let _cstring_compare (s1 : _cstring) (s2 : _cstring) = String.compare s1 s2
  end