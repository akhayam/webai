module SMap = Map.Make(String)
module type FT_imp_server = functor (FTL : Language.FT_lang_imp) (FTE : Expression.FT_lang_expr) (FTV : Value.FT_lang_values)(FTD : Device.FT_lang_device)(FTB : Base_types.FT_base_types) (CT : Concrete_types.concrete_types) -> sig
  open FTL(FTE)(FTV)(FTB)(CT)
  type hostName
  type server_value_ext =
    | Closure of tilde_language
  and server_value =
     server_value_ext FTV(FTB)(CT).ext_v
  and server_expr_ext =
    | Tilde of tilde_language
  and server_expr =
    (server_expr_ext, server_value_ext) FTE(FTV)(FTB)(CT).ext_e
  and server_language =
    (ext_server, server_expr_ext, server_value_ext) FTL(FTE)(FTV)(FTB)(CT).ext_imp
  and ext_server =
    | Act of (FTD(FTV)(FTB)(CT).actuator * FTD(FTV)(FTB)(CT).device_id * FTD(FTV)(FTB)(CT).permission)
    | Get of (FTE(FTV)(FTB)(CT).var * FTD(FTV)(FTB)(CT).device_id * FTD(FTV)(FTB)(CT).permission)
    (*| Wait of FTE(FTV)(FTB)(CT).var*)
  and tilde_value =
    FTV(FTB)(CT).not_implemented FTV(FTB)(CT).ext_v
  and tilde_expr_ext =
    | Dollar of FTE(FTV)(FTB)(CT).var
  and tilde_expr =
    (tilde_expr_ext, tilde_value) FTE(FTV)(FTB)(CT).ext_e
  and dollarOrVal =
    | D of tilde_expr
    | V of tilde_value
  and tilde_language_ext =
    | Call of (FTD(FTV)(FTB)(CT).url * dollarOrVal * (FTE(FTV)(FTB)(CT).var * tilde_language))
  and tilde_language =
    (tilde_language_ext, tilde_expr_ext,FTV(FTB)(CT).not_implemented) ext_imp
  (*val server_eval_expression : (server_expr_ext,server_value_ext) FTE(FTV)(FTB)(CT).eval_expression*)
end

module F_imp_server = functor (FTL : Language.FT_lang_imp) (FTE : Expression.FT_lang_expr) (FTV : Value.FT_lang_values)(FTD : Device.FT_lang_device)(FTB : Base_types.FT_base_types) (CT : Concrete_types.concrete_types) -> struct
   include CT
   include FTB(CT)
   include FTD(FTV)(FTB)(CT)
   include FTV(FTB)(CT)
   include FTE(FTV)(FTB)(CT)
   include FTD(FTV)(FTB)(CT)
   include FTL(FTE)(FTV)(FTB)(CT)
   type hostName = string
   type server_value_ext =
     | Closure of tilde_language
   and server_value =
      server_value_ext ext_v
   and server_expr_ext =
     | Tilde of tilde_language
   and server_expr =
     (server_expr_ext, server_value_ext) ext_e
   and server_language =
     (ext_server, server_expr_ext, server_value_ext) ext_imp
   and ext_server =
     | Act of (actuator * device_id * permission)
     | Get of (var * device_id * permission)
     (*| Wait of var *)
   and tilde_value =
     not_implemented ext_v
   and tilde_expr_ext =
     | Dollar of var
   and tilde_expr =
     (tilde_expr_ext, tilde_value) ext_e
   and dollarOrVal =
     | D of tilde_expr
     | V of tilde_value
   and tilde_language_ext =
     | Call of (url * dollarOrVal * (var * tilde_language))
   and tilde_language =
     (tilde_language_ext, tilde_expr_ext,not_implemented) ext_imp
(*let rec server_eval_expression (e : server_expr) (m : server_value SMap.t) : server_value =
  (match e with
  | StdExpr se ->
     (match se with
     | Value v -> v
     | Var x ->
        (match SMap.find_opt x m with | Some v -> v | None -> failwith ("variable "^x^" does not have a binding"))
     | Binary (o,e1,e2) ->
        (match o with
        | Add -> (match (server_eval_expression e1 m, server_eval_expression e2 m) with
                 | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Nat (_add n1 n2))
                 | _ -> failwith "+ takes only nat parameters")
        | Sub -> (match (server_eval_expression e1 m, server_eval_expression e2 m) with
                 | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Nat (_sub n1 n2))
                 | _ -> failwith "- takes only nat parameters")
        | Mul -> (match (server_eval_expression e1 m, server_eval_expression e2 m) with
                 | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Nat (_mul n1 n2))
                 | _ -> failwith "* takes only nat parameters")
        | L -> (match (server_eval_expression e1 m, server_eval_expression e2 m) with
                 | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Bool (_l n1 n2))
                 | _ -> failwith "< takes only nat parameters")
        | LEq -> (match (server_eval_expression e1 m, server_eval_expression e2 m) with
                 | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Bool (_le n1 n2))
                 | _ -> failwith "<= takes only nat parameters")
        | G -> (match (server_eval_expression e1 m, server_eval_expression e2 m) with
                 | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Bool (_g n1 n2))
                 | _ -> failwith "> takes only nat parameters")
        | GEq -> (match (server_eval_expression e1 m, server_eval_expression e2 m) with
                 | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Bool (_ge n1 n2))
                 | _ -> failwith ">= takes only nat parameters")
        | Eq -> (match (server_eval_expression e1 m, server_eval_expression e2 m) with
                 | (StdVal (Nat n1), StdVal (Nat n2)) -> StdVal (Bool (_ieq n1 n2))
                 | (StdVal (Bool b1), StdVal (Bool b2)) -> StdVal (Bool (_beq b1 b2))
                 | (StdVal (Str s1), StdVal (Str s2)) -> StdVal (Bool (_seq s1 s2))
                 | (StdVal Unit, StdVal Unit) -> StdVal (Bool _true)
                 | (StdVal Undefined, StdVal Undefined) -> StdVal (Bool _false)
                 | _ -> failwith "= does not allow mixed expression")
        | And -> (match (server_eval_expression e1 m, server_eval_expression e2 m) with
                 | (StdVal (Bool b1), StdVal (Bool b2)) -> StdVal (Bool (_and b1 b2))
                 | _ -> failwith "&& takes only bool parameters")
        | Or -> (match (server_eval_expression e1 m, server_eval_expression e2 m) with
                 | (StdVal (Bool b1), StdVal (Bool b2)) -> StdVal (Bool (_or b1 b2))
                 | _ -> failwith "|| takes only bool parameters"))
     | Unary (o,e1) -> match o with
                       | Neg ->
                          match server_eval_expression e1 m with
                          | StdVal (Nat n) -> StdVal (Nat (_mul n (mk_nat (-1))))
                          | StdVal (Bool b) -> if is_true (_beq b _true) then StdVal (Bool (_false)) else StdVal (Bool (_true))
                          | _ -> failwith "! takes only bool and nat")
  | ExtExpr _ -> StdVal Undefined) (* to define as soon as I put extensions of expressions as parameter of the Functor*)*)

end

module SERVER_IMP = F_imp_server(Language.F_lang_imp)(Expression.F_lang_expr)(Value.F_lang_values)(Device.F_lang_device)(Base_types.F_base_types)(Concrete_types.CONCRETE_TYPES)

module SERVER_IMP_SYM = F_imp_server(Language.F_lang_imp)(Expression.F_lang_expr)(Value.F_lang_values)(Device.F_lang_device)(Base_types.F_base_types)(Concrete_types.Z3_TYPES)
