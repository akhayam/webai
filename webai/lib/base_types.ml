(* decoupling type base type declaration from actual instantiation *)

(* base_types functor type*)
module type FT_base_types = functor (CT : Concrete_types.concrete_types) -> sig
  open CT

  type _bool = _cbool
  type _int = _cint
  type _string = _cstring

  val _true : _bool
  val _false : _bool
  val is_true : _bool -> bool

  val _add : _int -> _int -> _int
  val _sub : _int -> _int -> _int
  val _mul : _int -> _int -> _int

  val _and : _bool -> _bool -> _bool
  val _or : _bool -> _bool -> _bool

  val _l : _int -> _int -> _bool
  val _g : _int -> _int -> _bool
  val _le : _int -> _int -> _bool
  val _ge : _int -> _int -> _bool
  val _ieq : _int -> _int -> _bool
  val _beq : _bool -> _bool -> _bool
  val _seq : _string -> _string -> _bool
  val _bool_compare : _bool -> _bool -> int
  val _int_compare : _int -> _int -> int
  val _string_compare : _string -> _string -> int
end

(* base_types functor *)
module F_base_types = functor (CT : Concrete_types.concrete_types) -> struct
  include CT

  type _bool = CT._cbool
  type _int = CT._cint
  type _string = CT._cstring

  let _true = CT._ctrue
  let _false = CT._cfalse
  let is_true = CT._cisTrue

  let _add = _cadd
  let _sub = _csub
  let _mul = _cmul

  let _and = _cand
  let _or = _cor

  let _l = _cl
  let _g = _cg
  let _le = _cle
  let _ge = _cge
  let _ieq = _cieq
  let _beq = _cbeq
  let _seq = _cseq
  let _bool_compare = CT._cbool_compare
  let _int_compare = CT._cint_compare
  let _string_compare = CT._cstring_compare
end

module BT_INT = F_base_types(Concrete_types.CONCRETE_TYPES)
module BT_SYM = F_base_types(Concrete_types.Z3_TYPES)