(* lang_imp functor type *)
module type FT_lang_imp = functor (FTE : Expression.FT_lang_expr) (FTV : Value.FT_lang_values) (FTB : Base_types.FT_base_types) (CT : Concrete_types.concrete_types) -> sig
  open FTE(FTV)(FTB)(CT)

  type ('a,'b,'c) standard_imp =
  | Assignment of (var * ('b,'c) ext_e)
  | Sequence of (('a,'b,'c) ext_imp * ('a,'b,'c) ext_imp)
  | If of (('b,'c) ext_e * ('a,'b,'c) ext_imp * ('a,'b,'c) ext_imp)
  | While of (('b,'c) ext_e * ('a,'b,'c) ext_imp)
  | Skip
  | Return of 'b
  and ('a,'b,'c) ext_imp =
  | StdLan of ('a,'b,'c) standard_imp
  | ExtLan of 'a
end


(* lang_imp functor *)
module F_lang_imp = functor (FTE : Expression.FT_lang_expr) (FTV : Value.FT_lang_values) (FTB : Base_types.FT_base_types) (CT : Concrete_types.concrete_types) -> struct
  include CT
  include FTB(CT)
  include FTV(FTB)(CT)
  include FTE(FTV)(FTB)(CT)

  type ('a,'b,'c) standard_imp =
  | Assignment of (var * ('b,'c) ext_e)
  | Sequence of (('a,'b,'c) ext_imp * ('a,'b,'c) ext_imp)
  | If of (('b,'c) ext_e * ('a,'b,'c) ext_imp * ('a,'b,'c) ext_imp)
  | While of (('b,'c) ext_e * ('a,'b,'c) ext_imp)
  | Skip
  | Return of 'b
  and ('a,'b,'c) ext_imp =
  | StdLan of ('a,'b,'c) standard_imp
  | ExtLan of 'a
end

(* IMP instantiation *)

module IMP_init = F_lang_imp(Expression.F_lang_expr)(Value.F_lang_values)(Base_types.F_base_types)(Concrete_types.CONCRETE_TYPES)