module type FT_imp_client = functor (FTL : Language.FT_lang_imp) (FTE : Expression.FT_lang_expr) (FTV : Value.FT_lang_values)(FTD : Device.FT_lang_device)(FTB : Base_types.FT_base_types) (CT : Concrete_types.concrete_types) -> sig
  open FTL(FTE)(FTV)(FTB)(CT)
  type client_id
  type client_value =
      FTV(FTB)(CT).not_implemented FTV(FTB)(CT).ext_v
  and client_expr =
     (FTV(FTB)(CT).not_implemented, client_value) FTE(FTV)(FTB)(CT).ext_e
  and ext_client =
  | Call of (FTD(FTV)(FTB)(CT).url * client_value *
             (FTE(FTV)(FTB)(CT).var * (ext_client, client_expr, client_value) ext_imp))
  and client_language =
     (ext_client, client_expr, client_value) ext_imp
end

module F_imp_client = functor (FTL : Language.FT_lang_imp) (FTE : Expression.FT_lang_expr) (FTV : Value.FT_lang_values)(FTD : Device.FT_lang_device)(FTB : Base_types.FT_base_types) (CT : Concrete_types.concrete_types) -> struct
   include CT
   include FTB(CT)
   include FTD(FTV)(FTB)(CT)
   include FTV(FTB)(CT)
   include FTD(FTV)(FTB)(CT)
   include FTE(FTV)(FTB)(CT)
   include FTL(FTE)(FTV)(FTB)(CT)
  type client_id = int
  type client_value =
     not_implemented ext_v
  and client_expr =
     (not_implemented, client_value) ext_e
  and ext_client =
  | Call of (url * client_value *
             (var * (ext_client, client_expr, client_value) ext_imp))
  and client_language =
     (ext_client, client_expr, client_value) ext_imp
end

module CLIENT_IMP = F_imp_client(Language.F_lang_imp)(Expression.F_lang_expr)(Value.F_lang_values)(Device.F_lang_device)(Base_types.F_base_types)(Concrete_types.CONCRETE_TYPES)

module CLIENT_IMP_SYM = F_imp_client(Language.F_lang_imp)(Expression.F_lang_expr)(Value.F_lang_values)(Device.F_lang_device)(Base_types.F_base_types)(Concrete_types.Z3_TYPES)
open CLIENT_IMP

let f (a : device_id) : device_location  =
    match a with
   | (_,l) -> l
