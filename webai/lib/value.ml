
(*
module type Ext_lang_values = sig
   type t
   val ext_val_compare : t -> t -> int
 end
 
 module Not_implemented = struct
   type t = Not_implemented
   let ext_val_compare (_ : t) (_ : t) = 0
 end
*)

(* value extensions have to be provided as modules*)
(* lang_values functor type *)
module type FT_lang_values = functor (FTB : Base_types.FT_base_types) (CT : Concrete_types.concrete_types) -> sig
   open FTB(CT)
   
   type standard_v =
   | Nat of _int
   | Bool of _bool
   | Str of _string
   | Unit
   | Undefined
   and 'a ext_v =
   | ExtVal of 'a
   | StdVal of standard_v
   and not_implemented = | Not_Implemented

   val value_compare : 'a ext_v -> 'a ext_v -> int (* to implement in order to abstract code*)
end

(* lang_values Functor *)
module F_lang_values = functor (FTB : Base_types.FT_base_types) (CT : Concrete_types.concrete_types) -> struct
  include CT
  include FTB(CT)

   type standard_v =
   | Nat of FTB(CT)._int
   | Bool of FTB(CT)._bool
   | Str of FTB(CT)._string
   | Unit
   | Undefined
   and 'a ext_v =
   | ExtVal of 'a
   | StdVal of standard_v
   and not_implemented = | Not_Implemented
   let value_compare (v1 : 'a ext_v) (v2 : 'a ext_v) : int =
      match (v1,v2) with
      | (ExtVal _,ExtVal _) -> 1 (*for now I don't care about extVals*)
      | (StdVal v1, StdVal v2) ->(
         match (v1,v2) with
         | (Nat n1, Nat n2) -> _int_compare n1 n2
         | (Bool b1, Bool b2) -> _bool_compare b1 b2
         | (Str s1, Str s2) -> _string_compare s1 s2
         | (Unit, Unit) -> 1
         | (Undefined, Undefined) -> 1
         | _ -> 0)
      | _ -> 0
end

(* Values instantitation *)

module V_inst = F_lang_values(Base_types.F_base_types)(Concrete_types.CONCRETE_TYPES)
