open Aiwebi.Structures

module P_Int = Aiwebi.Parser
module L_Int = Aiwebi.Lexer
(*open Aiwebi.Op*)

module Exec_Concrete (WI : Aiwebi.Webi.WEBI_INTERPRETER) = struct
  
  module DST = WI.WO.SI.ST
  module CST = WI.CI.ST
  module SL = WI.WO.SI.ST.SLAN
  module CVAR = WI.WO.SI.ST.CLAN.CVAR
  module SVAR = WI.WO.SI.ST.SLAN.SVAR


  let parse prg 
    (intro : (Lexing.lexbuf -> P_Int.token) -> Lexing.lexbuf -> WI.program)
    : WI.program =
    let lp = Lexing.from_string prg in
    intro L_Int.token lp

  let rec split_tiers (prg : WI.program) services clients devices =
    (match prg with
    | [] -> (services,clients,devices)
    | x::t -> 
      (match x with
      | Service p1 -> split_tiers t ([p1]@services) clients devices
      | Client p1 -> split_tiers t services ([p1]@clients) devices
      | Device p1 -> split_tiers t services clients ([p1]@devices)
      | Oracle _ -> failwith "ERROR : World Oracle not implemented"))

  let mk_webservices services =
    match services with
    | [] -> SMap.empty
    | _ ->
      List.fold_left
        (fun map' x -> (match x with | (url,_,x,p) ->
          SMap.add url (x,p) map'))
        SMap.empty
        services
      
  let mk_inits clients =
    match clients with
    | [] -> WI.InitSet.empty
    | _ ->
      List.fold_left
        (fun set x -> (match x with | (url, t, i) ->
                        WI.InitSet.add (url,t,i) set))
        WI.InitSet.empty
        clients
      
  let mk_devices devices : WI.device SSMap.t  =
    match devices with
    | [] -> SSMap.empty
    | _ ->
      List.fold_left
        (fun ssmap d -> match d with | ((dname,ldev),v,sem) ->
                          SSMap.add (dname,ldev) 
                                    {
                                      WI.state = v;
                                      semantics =
                                        List.fold_left
                                          (fun smap s -> match s with | (a,v) ->
                                          SMap.add a v smap)
                                          SMap.empty
                                          sem}
                                    ssmap)
      SSMap.empty
      devices
      
  let empty_services = 
    {WI.srv_run = IIMap.empty;
    srv_done=IIMap.empty;
    srv_act=IIMap.empty;
    srv_get=IIMap.empty;}
  let empty_clients =
    {WI.cl_run = IMap.empty;
    cl_done=IMap.empty;
    cl_call=IMap.empty;
    cl_boot=IMap.empty}
        
      
  let program_to_conf p wo (intro : (Lexing.lexbuf -> P_Int.token) ->
      Lexing.lexbuf -> WI.program) =
    let (services,clients,devices) = split_tiers (parse p intro) [] [] [] in
    {
      WI.wl = [];
      inits = mk_inits clients;
      services = empty_services;
      clients = empty_clients;
      devices = mk_devices devices;
      webservices = mk_webservices services;
      wo = wo;
      debug = []    
    }
  let exec p wo (intro : (Lexing.lexbuf -> P_Int.token) -> Lexing.lexbuf -> WI.program) =
    let c = program_to_conf p wo intro in
    let c' = WI.execute_webi [c] Init 1 in
    Printf.printf "Number of Execution Traces = %i\n" (List.length c');
    WI.observe_actuation c' ("window","kitchen") "open";
    (List.map  (fun (c,_,_) -> 
                  Printf.printf "##############################################################################################################\n";
                  Printf.printf "\n~~~~ Start World Trace ~~~~\n\n";
                  WI.WO.print_pe_list (WI.get_world_log c);
                  Printf.printf "\n~~~~ End World Trace ~~~~\n";
                  Printf.printf "\n--- Start Execution Trace ----\nScheduler's Iteration #1\n\n";
                  WI.print_trace_element_list (WI.get_debug c)(*; opt_apply WI.WO.SI.ST.top []*))
              c')    
end

module P_Abstr = Aiwebi.Parser_abstr
module L_Abstr = Aiwebi.Lexer_abstr

module Exec_Abstr (WI : Aiwebi.Webi.WEBI_INTERPRETER) = struct
  
  module DST = WI.WO.SI.ST
  module CST = WI.CI.ST
  module SL = WI.WO.SI.ST.SLAN
  module CVAR = WI.WO.SI.ST.CLAN.CVAR
  module SVAR = WI.WO.SI.ST.SLAN.SVAR


  let parse prg 
    (intro : (Lexing.lexbuf -> P_Abstr.token) -> Lexing.lexbuf -> WI.program)
    : WI.program =
    let lp = Lexing.from_string prg in
    intro L_Abstr.token lp

  let rec split_tiers (prg : WI.program) services clients devices =
    (match prg with
    | [] -> (services,clients,devices)
    | x::t -> 
      (match x with
      | Service p1 -> split_tiers t ([p1]@services) clients devices
      | Client p1 -> split_tiers t services ([p1]@clients) devices
      | Device p1 -> split_tiers t services clients ([p1]@devices)
      | Oracle _ -> failwith "ERROR : World Oracle not implemented"))

  let mk_webservices services =
    match services with
    | [] -> SMap.empty
    | _ ->
      List.fold_left
        (fun map' x -> (match x with | (url,_,x,p) ->
          SMap.add url (x,p) map'))
        SMap.empty
        services
      
  let mk_inits clients =
    match clients with
    | [] -> WI.InitSet.empty
    | _ ->
      List.fold_left
        (fun set x -> (match x with | (url, t, i) ->
                        WI.InitSet.add (url,t,i) set))
        WI.InitSet.empty
        clients
      
  let mk_devices devices : WI.device SSMap.t  =
    match devices with
    | [] -> SSMap.empty
    | _ ->
      List.fold_left
        (fun ssmap d -> match d with | ((dname,ldev),v,sem) ->
                          SSMap.add (dname,ldev) 
                                    {
                                      WI.state = v;
                                      semantics =
                                        List.fold_left
                                          (fun smap s -> match s with | (a,v) ->
                                          SMap.add a v smap)
                                          SMap.empty
                                          sem}
                                    ssmap)
      SSMap.empty
      devices
      
  let empty_services = 
    {WI.srv_run = IIMap.empty;
    srv_done=IIMap.empty;
    srv_act=IIMap.empty;
    srv_get=IIMap.empty;}
  let empty_clients =
    {WI.cl_run = IMap.empty;
    cl_done=IMap.empty;
    cl_call=IMap.empty;
    cl_boot=IMap.empty}
        
      
  let program_to_conf p wo (intro : (Lexing.lexbuf -> P_Abstr.token) ->
      Lexing.lexbuf -> WI.program) =
    let (services,clients,devices) = split_tiers (parse p intro) [] [] [] in
    {
      WI.wl = [];
      inits = mk_inits clients;
      services = empty_services;
      clients = empty_clients;
      devices = mk_devices devices;
      webservices = mk_webservices services;
      wo = wo;
      debug = []    
    }
  let exec p wo (intro : (Lexing.lexbuf -> P_Abstr.token) ->
      Lexing.lexbuf -> WI.program) =
    let c = program_to_conf p wo intro in    
    let c' = WI.execute_webi [c] Init 1 in
    Printf.printf "\n\n\nNumber of 1st Abstract Execution Traces = %i\n" (List.length c');
    WI.observe_actuation c' ("window","kitchen") "open";
    (List.map  (fun (c,_,_) -> 
                  Printf.printf "##############################################################################################################\n";
                  Printf.printf "\n~~~~ Start World Trace ~~~~\n\n";
                  WI.WO.print_pe_list (WI.get_world_log c);
                  Printf.printf "\n~~~~ End World Trace ~~~~\n";
                  Printf.printf "\n--- Start Execution Trace ----\nScheduler's Iteration #1\n\n";
                  WI.print_trace_element_list (WI.get_debug c)(*; opt_apply WI.WO.SI.ST.top []*))
              c')    
end

module EAbstr = Exec_Abstr(Aiwebi.Webi.Webi_AInterpreter)


let oven_window = "
    device window kitchen 0 (
      {open -> 1}
      {close -> 0}
    )
    device oven kitchen 0 (
      {on -> 1}
      {off -> 0}
      {cooling -> 2}
    )

    device thermometer kitchen 23 ()

    service ovenManager :: A (x) :=
      get(status,(oven,kitchen),\"permission_oven\");
      if (status = 2) {
        act(cooling,(oven,kitchen),\"permission_oven\");
      } else {
        skip;
      };

    service ovenOn :: B (x) :=
      act(on,(oven,kitchen),\"permission_oven\");
      act(off,(oven,kitchen),\"permission_oven\");
    
    service windowManager :: A (x) :=
      get(temp,(thermometer,kitchen),\"permission_temp\");
      if (temp >= 25) {
        act(open,(window,kitchen),\"permission_window\");
      } else {
        skip;
      };
    
    client 1 calls (ovenOn) := num
    client 2 calls (windowManager) := num
    client 3 calls (ovenManager) := num
"

let oven_window_sec = "
    device window kitchen 0 (
      {open -> 1}
      {close -> 0}
    )
    device oven kitchen 0 (
      {on -> 1}
      {off -> 0}
      {cooling -> 2}
    )

    device thermometer kitchen 23 ()

    service ovenManager :: A (x) :=
      get(status,(oven,kitchen),\"permission_oven\");
      if (status = 2) {
        act(cooling,(oven,kitchen),\"permission_oven\");
      } else {
        skip;
      };

    service ovenOn :: B (x) :=
      act(on,(oven,kitchen),\"permission_oven\");
      act(off,(oven,kitchen),\"permission_oven\");

    service windowManager :: A (x) :=
      get(temp,(thermometer,kitchen),\"permission_temp\");
      if (temp >= 25) {
        get(status,(oven,kitchen),\"\");
        if (status = 0){
          act(open,(window,kitchen),\"permission_window\");
        } else {
          skip;
        };
      } else {
        skip;
      };
    
    client 1 calls (ovenOn) := num
    client 2 calls (windowManager) := num
    client 3 calls (ovenManager) := num
"


module EInt = Exec_Concrete(Aiwebi.Webi.Webi_Interpreter_Int)

let wo_oven_window = 
  SSMap.add ("window","kitchen") (SMap.add "open" [] (SMap.add "close" [] SMap.empty))
    (SSMap.add ("oven","kitchen") 
       (SMap.add "off" 
                 [(("thermometer","kitchen"),(fun (mv : EInt.DST.value) :
                    EInt.DST.value ->
                    EInt.DST.SNum (
                      EInt.DST.NUM.(-)
                        (match mv with
                        | SNum mn -> mn
                        | _ -> failwith "ERRORE : cannot sum bools")
                        (EInt.DST.NUM.mk_const ~i0:(Some 0) 4))));
                  (("oven","kitchen"),(fun (_ : EInt.DST.value) : EInt.DST.value ->
                    EInt.DST.SNum (EInt.DST.NUM.of_int 2)
                    ))]
                 (SMap.add "on" 
                           [(("thermometer","kitchen"),(fun (mv : EInt.DST.value) -> 
                           EInt.DST.SNum (
                            EInt.DST.NUM.(+)
                            (match mv with
                            | SNum mn -> mn
                            | _ -> failwith "ERRORE : cannot sum bools")
                            (EInt.DST.NUM.mk_const ~i0:(Some 0) 4))))] 
                            (SMap.add "cooling"
                                     [(("thermometer","kitchen"),
                                     (fun (mv : EInt.DST.value) -> 
                                      EInt.DST.SNum (
                                        EInt.DST.NUM.(--)
                                      (match mv with
                                      | SNum mn -> mn
                                      | _ -> failwith "ERRORE : cannot sum bools")
                                    (EInt.DST.NUM.mk_const ~i0:(Some 0) 4))));
                                    (("oven","kitchen"),(fun (_ : EInt.DST.value) :
                                      EInt.DST.value ->
                                      EInt.DST.SNum (EInt.DST.NUM.of_int 0)
                                      ))] SMap.empty)))
                           SSMap.empty)


let a_wo_oven_window = 
  SSMap.add ("window","kitchen") (SMap.add "open" [] (SMap.add "close" [] SMap.empty))
    (SSMap.add ("oven","kitchen") 
       (SMap.add "off" 
                 [(("thermometer","kitchen"),(fun (mv : EAbstr.DST.value) :
                  EAbstr.DST.value ->
                  EAbstr.DST.SNum (
                    EAbstr.DST.NUM.(-)
                        (match mv with
                        | SNum mn -> mn
                        | _ -> failwith "ERRORE : cannot sum bools")
                        (EAbstr.DST.NUM.mk_const ~i0:(Some 0) 4))));
                  (("oven","kitchen"),(fun (_ : EAbstr.DST.value) :
                    EAbstr.DST.value ->
                    EAbstr.DST.SNum (EAbstr.DST.NUM.of_int 2)
                    ))
                  ]
                 (SMap.add "on" 
                           [(("thermometer","kitchen"),
                           (fun (mv : EAbstr.DST.value) -> 
                            EAbstr.DST.SNum (
                              EAbstr.DST.NUM.(+)
                            (match mv with
                            | SNum mn -> mn
                            | _ -> failwith "ERRORE : cannot sum bools")
                            (EAbstr.DST.NUM.mk_const ~i0:(Some 0) 4))))] 
                            (SMap.add "cooling"
                                     [(("thermometer","kitchen"),
                                     (fun (mv : EAbstr.DST.value) -> 
                                     EAbstr.DST.SNum (
                                      EAbstr.DST.NUM.(--)
                                      (match mv with
                                      | SNum mn -> mn
                                      | _ -> failwith "ERRORE : cannot sum bools")
                                    (EAbstr.DST.NUM.mk_const ~i0:(Some 0) 4))));
                                    (("oven","kitchen"),(fun (_ : EAbstr.DST.value) :
                                      EAbstr.DST.value ->
                                      EAbstr.DST.SNum (EAbstr.DST.NUM.of_int 0)
                                      ))] SMap.empty)))
                           SSMap.empty)


(*co2 sensor ranges between 0 and 5. 3 is the threshold btw low and high co2 level*)
let listing_1 = "
    device co2 house 3 ()
    device switch co2 0 = (
      {on -> 1}
      {off -> 0}
    )

    service handleLevel :: A (x) =
      get(currentValue,(co2,house),\"\");
      get(active,(fan,house),\"\");
      if (currentValue >= 3) {
        if (active = 1) {
          act(on,(switch,co2),\"\");
        } else {
          skip;
        };
      } else {
        if (active = 1) {
          act(off,(switch,co2),\"\");
        } else {
          skip;
        };
      };
"

let _ = EInt.exec oven_window wo_oven_window P_Int.webis
let _ = EInt.exec oven_window_sec wo_oven_window P_Int.webis
let _ = EAbstr.exec oven_window a_wo_oven_window P_Abstr.webis
let _ = EAbstr.exec oven_window_sec a_wo_oven_window P_Abstr.webis