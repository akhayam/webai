module V = Value
module SMap = Structures.SMap
module Op = Op


let (let*) ((w,s) : ('a Option.t*string))(f : 'a -> 'b): 'b =
  match w with
  | Some v -> f v
  | None -> failwith s
let (let&) ((w,s) : ('a Option.t*string)) (f : 'a -> 'a): 'a =
  match w with
  | Some v -> f v
  | None -> failwith s
let opt_apply (fo: (('a -> 'b) Option.t * string)) (p : 'a): 'b = (let* f = fo in f p)
let opt_extr (w: ('a Option.t * string)): 'a = (let& v = w in v)

module type SERVER_STATE  =
  sig
    module SLAN : Language.SERVER_LANGUAGE
    module CLAN : Language.CLIENT_LANGUAGE
    module NUM : V.NUMBERS
    module BOOL : V.BOOLEANS
    module PA : Domain.PRE_APRON
    type t
    type key
    type value = | SNum of NUM.t | SBool of BOOL.t | SClos of CLAN.block
    val man : ((PA.t Apron.Manager.t) Option.t*string)
    val key_of_string : string -> key
    val key_to_string : key -> string
    val update : t -> key -> value -> t
    val find : t -> key -> value
    val to_state_value : SLAN.SVAL.t -> value
    val new_state :  t
    val make : SLAN.block -> string -> value -> t
    val is_true : value -> bool
    val value_compare : value -> value -> int
    val to_string : value -> string
    val top : ((Variable.Server_Variable.t list -> t) Option.t * string)
    val is_sat : ((t -> bool) Option.t*string)
    val is_bottom : ((t -> bool) Option.t * string)
    val is_leq : ((t -> t -> bool) Option.t*string) 
    val widen : ((t -> t -> t) Option.t*string) 
    val join : ((t -> t -> t) Option.t*string)
    val post_assign : ((SLAN.SVAR.t -> SLAN.expr -> t -> t) Option.t*string)
    val post_cond : ((SLAN.expr -> t -> t) Option.t * string)
    val mk_init_value : CLAN.CVAR.typ -> value
  end


module Server_Concrete_State (SLAN : Language.SERVER_LANGUAGE)(CLAN : Language.CLIENT_LANGUAGE)(NUM : Domain.PRE_APRON -> V.NUMBERS)(BOOL : V.BOOLEANS) (PA : Domain.PRE_APRON): SERVER_STATE =
  struct
    module SLAN = SLAN
    module CLAN = CLAN
    module NUM = NUM(PA)
    module BOOL = BOOL
    module SVAR = Variable.Server_Variable
    module PA = PA
    type key = string
    type value = | SNum of NUM.t | SBool of BOOL.t | SClos of CLAN.block
    type t = value SMap.t
    let man = (None,"ERROR : concrete state does not have a domain manager")
    let key_of_string (k : string) : key = k
    let key_to_string (k : key) : string = k
    let to_state_value (sv : SLAN.SVAL.t) : value =
      match sv with
      | VNum i -> SNum (NUM.of_int i)
      | VBool b  -> SBool (BOOL.of_bool b)
      | _ -> failwith "ERROR" 
    let update (m : t) (k : key) (v : value) : t =
      SMap.add k v m
    let find (m : t) (k : key) : value =
      SMap.find k m
    let new_state : t = SMap.empty

    let make (_ : SLAN.block) (init_var : string) (v: value) : t =
      update new_state init_var v
    let is_true (v : value) : bool =
      match v with
      | SBool b -> BOOL.is_true b
      | _ -> false
    let value_compare v1 v2 =
      match (v1,v2) with
      | (SNum i1, SNum i2) -> NUM.compare i1 i2
      | (SBool i1, SBool i2) -> BOOL.compare i1 i2
      | (SNum _, SBool _) -> 1
      | _ -> -1
    let to_string (v : value) : string =
      match v with
      | SNum i -> "NUM "^(NUM.to_string i)
      | SBool b -> "BOOL "^(BOOL.to_string b)
      | _ -> "CLOSURE"
    let top : ((SVAR.t list -> t)  Option.t * string) = (None, "ERROR : top is not a concrete state value")
    let is_sat : ((t -> bool) Option.t*string) = (None,"ERROR : is_sat not implemented for the concrete state")
    let is_bottom : ((t -> bool) Option.t*string) = (None,"ERROR : is_bottom not implemented for the concrete state")
    let is_leq : ((t -> t -> bool) Option.t*string) = (None,"ERROR : is_leq not implemented for the concrete state")
    let widen : ((t -> t -> t) Option.t*string) = (None,"ERROR : widen not implemented for the concrete state")
    let join : ((t -> t -> t) Option.t*string) = (None,"ERROR : join not implemented for the concrete state")
    let post_assign : ((SLAN.SVAR.t -> SLAN.expr -> t -> t) Option.t*string) = (None, "ERROR : post_assign not implemented for the concrete state")
    let post_cond : ((SLAN.expr -> t -> t) Option.t * string) = (None, "ERROR : post_assign not implemented for the concrete state")
    let mk_init_value (t: CLAN.CVAR.typ) : value =
      (match t with
      | Tnum ->  SNum (NUM.mk_const 10)
      | Tbool -> SBool (BOOL.mk_const ())
      | _ -> failwith "ERROR : init type cannot be unknown")
  end


module Server_Abstract_State (SLAN : Language.SERVER_LANGUAGE)(CLAN : Language.CLIENT_LANGUAGE)(NUM : Domain.PRE_APRON -> V.NUMBERS)(BOOL : V.BOOLEANS) (PA : Domain.PRE_APRON) : SERVER_STATE =
  struct
    module SLAN = SLAN
    module CLAN = CLAN
    module NUM = NUM(PA)
    module BOOL = BOOL
    module PA = PA
    module SVAR = Variable.Server_Variable
    module E = Apron.Environment
    module A = Apron.Abstract1
    module C = Apron.Coeff
    module S = Apron.Scalar
    module TE0 = Apron.Texpr0
    module TE1 = Apron.Texpr1
    module L1 = Apron.Lincons1
    module TC1 = Apron.Tcons1
    type value = | SNum of NUM.t | SBool of BOOL.t | SClos of CLAN.block
    let to_string (v : value) : string =
      match v with
      | SNum i -> "AbstrNUM "^(NUM.to_string i)
      | SBool b -> "AbstrBOOL "^(BOOL.to_string b)
      | _ -> "CLOSURE"
    let value_compare v1 v2 =
      match (v1,v2) with
      | (SNum i1, SNum i2) -> NUM.compare i1 i2
      | (SBool i1, SBool i2) -> BOOL.compare i1 i2
      | (SNum _, SBool _) -> 1
      | _ -> -1
    let to_state_value (sv : SLAN.SVAL.t) : value =
      match sv with
      | VNum i -> SNum (NUM.of_int i)
      | VBool b  -> SBool (BOOL.of_bool b)
      | _ -> failwith "ERROR"
    let is_true (v : value) : bool =
      match v with
      | SBool b -> BOOL.is_true b
      | _ -> false
    type u = PA.t
    type t = u A.t
    let man = (Some (PA.man),"")
    type key = Apron.Var.t
    let find (_ : t) (_ : key) : value =
      failwith "ERROR : No find for the abstract state"
    let key_of_string (k : string) : key = Apron.Var.of_string k
    let key_to_string (k : key) : string = (Apron.Var.to_string k)
    let top : ((SVAR.t list -> t)  Option.t * string) =
    (Some (
      fun (l : SVAR.t list) : t ->
        let l = List.map (fun (x : SVAR.t) -> Apron.Var.of_string x.vname) l in
        let env = E.make (Array.of_list l) [| |] in
        A.top (opt_extr man) env),
    "")
    let new_state : t = opt_apply top [{vname = "x"; vtyp = Tnum}]
    let is_sat : ((t -> bool) Option.t*string) = (Some (
      fun (a : t) ->
        Ailanguage_utils.linconsarray_sat (A.to_lincons_array (opt_extr man) a)),"")
    let is_bottom : ((t -> bool) Option.t*string) = (Some (
      fun a ->
        A.is_bottom (opt_extr man) a
    ),"")
    let is_leq : ((t -> t -> bool) Option.t*string) = (Some (
      fun (a0 :t) -> fun (a1 : t) ->
        A.is_leq (opt_extr man) a0 a1
    ),"")
    let widen : ((t -> t -> t) Option.t*string) = (Some (
      fun (a0 :t) -> fun (a1 : t) ->
        A.widening (opt_extr man) a0 a1
    ),"")
    let join : ((t -> t -> t) Option.t*string) = (Some (
      fun (a0 :t) -> fun (a1 : t) ->
        A.join (opt_extr man) a0 a1
    ),"")
    (*let coeff_2str  : C.t -> string = function
  | C.Scalar s -> S.to_string s
  | C.Interval _ -> failwith "pp_coeff-interval"

let coeff_2int : C.t -> int = function
  | C.Scalar s ->
    int_of_float
      (match s with
      | Float f -> f
      | Mpqf f -> Mpqf.to_float f
      | Mpfrf f -> Mpfrf.to_float f)
  | C.Interval _ -> failwith "pp_coeff-interval"*)

(*#### Apron Ops to Ops ####*)
(*let apron_uop_2uop : TE0.unop -> Op.Op.uop = function
| TE0.Neg -> Neg
| _ -> failwith "ERROR : Texpr0 unop not supported"*)

(*let apron_arith_bop_2bop : TE0.binop -> Op.Op.bop = function 
  | TE0.Add -> Add
  | TE0.Sub -> Sub
  | TE0.Mul -> Mul
  | _ -> failwith "ERROR : binop unsopported by the language"*)

(*let apron_log_bop_2bop : L1.typ -> Op.Op.bop = function
  | L1.EQ -> Eq
  | L1.DISEQ -> NEq
  | L1.SUP -> GT
  | L1.SUPEQ -> GEq
  | _ -> failwith "ERROR : Lincons1 unsopported by the language"


(*#### PRINTERS ####*)
let apron_uop_2str : TE0.unop -> string = function
  | TE0.Neg -> "!"
  | _ -> failwith "ERROR : Texpr0 unop not supported"

let apron_arith_bop_2str : TE0.binop -> string = function
  | Add -> "+"
  | Sub -> "-"
  | Mul -> "*"
  | _ -> failwith "ERROR : Texpt0 binop not supported"

let apron_log_bop_2str : L1.typ -> string = function
| EQ -> "="
| DISEQ -> "!="
| SUP -> ">"
| SUPEQ -> ">="
| _ -> failwith "ERROR : Lincons1 unsopported in logop_2str"*)

(* Apron level 1 Expressions to strings *)
(*let apron_arith_expr_2str (te: TE1.t): string =
  let rec aux_expr (e: TE1.expr): string =
    match e with
    | Cst c -> coeff_2str c
    | Var v -> Apron.Var.to_string v
    | Unop (u, te0, _, _) ->
        Printf.sprintf "%s (%s)" (apron_uop_2str u) (aux_expr te0)
    | Binop (b, te0, te1, _, _) ->
        Printf.sprintf "(%s) %s (%s)"
          (aux_expr te0) (apron_arith_bop_2str b) (aux_expr te1) in
  aux_expr (TE1.to_expr te)*)

(* Debug pp for environments *)(*
  let environment_2str (env: E.t): string =
    let ai, af = E.vars env in
    (* Check no Real vars *)
    assert (Array.length af = 0);
    let isbeg = ref true in
    let s =
      Array.fold_left
        (fun (acc: string) (v: Apron.Var.t) ->
          let sep =
            if !isbeg then
              begin
                isbeg := false;
                ""
              end
            else "; " in
          Printf.sprintf "%s%s%s" acc sep (Apron.Var.to_string v)
        ) "" ai in
    Printf.sprintf "[|%s|]" s*)


(* Extraction of integer variables *)
(*let get_ivars (env: E.t): Apron.Var.t array =
  let ivars, fvars = E.vars env in
  if Array.length fvars > 0 then failwith "unimp: floating-point vars";
  ivars*)

(* Determines if set of constraints is SAT *)
  (*let linconsarray_sat (a: L1.earray): bool =
    let f_lincons (cons: L1.t) =
      if L1.is_unsat cons then false
      else true in
    (* Array of cons1 *)
    let ac1 =
      Array.mapi
        (fun i _ -> L1.array_get a i)
        a.L1.lincons0_array in
    Array.fold_left
      (fun (acc: bool) cons -> acc && (f_lincons cons)) true ac1*)
      
  (* Turns a set of constraints into a string *)
  (*let linconsarray_2stri (ind: string) (a: L1.earray): string =
    (* Extraction of the integer variables *)
    let env = a.L1.array_env in
    let ivars = get_ivars env in
    (* pretty-printing of a constraint *)
    let f_lincons (cons: L1.t): string =
      if L1.is_unsat cons then Printf.sprintf "%sUNSAT lincons\n" ind
      else
        (* print non zero coefficients *)
        let mt = ref false in
        let str0 =
          Array.fold_left
            (fun str v ->
              let c =
                try L1.get_coeff cons v
                with exn ->
                  failwith (Printf.sprintf "get_coeff, var %s, exn %s"
                              (Apron.Var.to_string v) (Printexc.to_string exn));
              in
              if not (C.is_zero c) then
                begin
                  let var = Apron.Var.to_string v in
                  let str_ext = Printf.sprintf "%s . %s" (coeff_2str c) var in
                  if !mt then
                    Printf.sprintf "%s + %s" str str_ext
                  else
                    begin
                      mt := true;
                      str_ext
                    end
                end
              else str
            ) "" ivars in
        (* print the constant *)
        let str1 =
          let d0 = coeff_2str (L1.get_cst cons) in
          if !mt then Printf.sprintf "%s + %s" str0 d0
          else d0 in
        (* print the relation *)
        Printf.sprintf "%s%s %s\n" ind str1
          (apron_log_bop_2str (L1.get_typ cons)) in
    (* Array of cons1 *)
    let ac1 =
      Array.mapi
        (fun i _ -> L1.array_get a i)
        a.L1.lincons0_array in
    Array.fold_left
      (fun (acc: string) cons ->
        Printf.sprintf "%s%s" acc (f_lincons cons)
      ) "" ac1*)
  
  (*let lincons_2expr (cons: L1.t) : SLAN.expr =
    if L1.is_unsat cons then
      begin
        Printf.printf "\nUNSAT lincons";
        Value (VBool false)
      end
    else
      let env = L1.get_env cons in
      let ivars = get_ivars env in
      let cst : SLAN.expr = Value (VNum (coeff_2int (L1.get_cst cons))) in
      let linexpr = Array.fold_left
        (fun expr v : SLAN.expr ->
          let c =
            try coeff_2int (L1.get_coeff cons v)
            with exn ->
              failwith (Printf.sprintf "get_coeff, var %s, exn %s"
                (Apron.Var.to_string v) (Printexc.to_string exn));
          in
          let v = SLAN.SVAR.make (Apron.Var.to_string v) SLAN.SVAR.Tnum in
          let m : SLAN.expr = match c with
          | 0 -> Value (VNum  0)
          | 1 -> Var v
          | _ -> BOp (Mul, Value (VNum c), Var v) in
          match m with
          | Value (VNum 0) -> expr
          | _ -> BOp (Add, m, expr))
        cst ivars
      in
      let op = apron_log_bop_2bop (L1.get_typ cons) in
      BOp (op, linexpr, Value (VNum 0))*)
  
  (* Translation into Apron expressions, with environment *)
  let translate_op : Op.Op.bop -> TE0.binop = function
    | Add -> TE1.Add
    | Sub -> TE1.Sub
    | Mul -> TE1.Mul
    | Eq -> failwith "ERROR: Eq"
    | _    -> failwith "binary operator, unsupported in translate_op"
  (*let translate_cond : Op.Op.bop -> L1.typ = function
    | Eq  -> TC1.EQ
    | NEq  -> TC1.DISEQ
    | LT  -> TC1.SUP
    | LEq  -> TC1.SUPEQ
    | _    -> failwith "condition, unsupported in translate_cond"*)
  let rec translate_expr (env: E.t) : SLAN.expr -> TE1.t = function
    | Value (VNum i) -> TE1.cst env (C.s_of_int i)
    | Value (VBool b) -> TE1.cst env (C.s_of_int (match b with | true -> 1 | false -> 0))
    | Value _ -> failwith "boolean constant, unsupported in translate_expr"
    | Var  v -> TE1.var env (Apron.Var.of_string v)
    | UOp  _ -> failwith "negation, unsupported in translate_expr"
    | BOp (b,e0,e1) ->
        TE1.binop
          (translate_op b)
          (translate_expr env e0)
          (translate_expr env e1) TE1.Int TE1.Near
    | Tilde _ -> failwith "unsopported tilde expression in translate_expr"
  and translate_cons (env: E.t) : SLAN.expr -> TC1.earray = function
    | Value (VBool b) ->
        if b then (* tautology constraint 0 = 0 *)
          let ex = translate_expr env (Value (VNum 0)) in
          let t = (TC1.make ex TC1.EQ) in
          {tcons0_array = [|t.tcons0|];array_env = t.env}
        else (* anti-tautology constraint 1 = 0 *)
          let ex = translate_expr env (Value (VNum 1)) in
          let t = TC1.make ex TC1.EQ in
          {tcons0_array = [|t.tcons0|];array_env = t.env}
    | BOp (Eq, e0, e1) ->
      let t = TC1.make (translate_expr env (BOp (Sub,e1,e0))) TC1.EQ in
      {tcons0_array = [|t.tcons0|];array_env = t.env}
    | BOp (NEq,e0,e1) ->
      let t = TC1.make (translate_expr env (BOp (Sub,e1, e0))) TC1.DISEQ in 
      {tcons0_array = [|t.tcons0|];array_env = t.env}
    | BOp (LEq, e0, e1) ->
      let t = TC1.make (translate_expr env (BOp (Sub,e1, e0))) TC1.SUPEQ in
      {tcons0_array = [|t.tcons0|];array_env = t.env}
    | BOp (LT,e0, e1) ->
      let t = TC1.make (translate_expr env (BOp (Sub,e1, e0))) TC1.SUP in
      {tcons0_array = [|t.tcons0|];array_env = t.env}
    | BOp (GEq,e0, e1) ->
      let t = TC1.make (translate_expr env (BOp (Sub,e0, e1))) TC1.SUPEQ in
      {tcons0_array = [|t.tcons0|];array_env = t.env}
    | BOp (GT,e0, e1) ->
      let t = TC1.make (translate_expr env (BOp (Sub,e0, e1))) TC1.SUP in
      {tcons0_array = [|t.tcons0|];array_env = t.env}
    (*| BOp (And,e0,e1) ->
      let t0 = (translate_cons env e0) in
      let t1 = (translate_cons env e1) in
      {tcons0_array = Array.append t0.tcons0_array t1.tcons0_array; array_env = E.lce t0.array_env t1.array_env}
    | BOp (Or,e0,e1) -> failwith "not handling or"*)
    | _ -> failwith "condition, unsupported"
  (*and modify_expr : SLAN.expr -> SLAN.expr = function
    | Value v -> Value v
    | BOp (EQ,e0,e1) ->  (BOp (Sub,e1,e0))*)

    let mk_init_value (t: CLAN.CVAR.typ) : value =
      (match t with
      | Tnum ->  SNum (opt_extr NUM.bottom)
      | Tbool -> SBool (BOOL.mk_const ())
      | _ -> failwith "ERROR : init type cannot be unknown")
    let post_assign : ((SLAN.SVAR.t -> SLAN.expr -> t -> t) Option.t*string) = (Some (
      fun x -> fun e -> fun a ->
      A.assign_texpr_array (opt_extr man) a
        [| Apron.Var.of_string x.vname |]
        [| translate_expr (A.env a) e |] None),"")
    let post_cond : ((SLAN.expr -> t -> t ) Option.t * string) =
      (Some (
        fun e -> fun a ->
          let env = A.env a in
          (*let eacons = TC1.array_make env 1 in*)
          let eacons = translate_cons env e in
          (*TC1.array_set eacons 0 c;*)
          A.meet_tcons_array (opt_extr man)  a eacons),"")
    (*let to_num_atyp (typ : PA.atyp) : NUM.PA.atyp =
      match typ with
      | BOX -> NUM.PA.BOX
      | OCT -> NUM.PA.OCT
      | POLKA -> NUM.PA.POLKA
      | POLKA_EQ -> NUM.PA.POLKA_EQ*)
    let to_texpr (v : value) (e : E.t) (typ : NUM.PA.atyp) : TE1.t =
        (match v with
        | SNum i -> TE1.of_expr e (Cst (NUM.to_coeff i typ))
        | _ -> failwith "ERROR : Booleans and Closures are not abstracted")

    let update (a : t) (k : key) (v : value) : t =
      A.assign_texpr_array (opt_extr man) a
      [| k |] [| (to_texpr v (A.env a) (NUM.PA.typ)) |] None

    let rec remove_duplicates l =
      let rec contains l n =
        match l with
        | [] -> false
        | h :: t ->
          h = n || contains t n
      in
      match l with
      | [] -> []
      | h :: t ->
        let acc = remove_duplicates t in
        if contains acc h then acc else h :: acc

    let make (p : SLAN.block) (init_var : string) (v: value) : t =
      let set_var = (SLAN.vars p) in
      let list_var = remove_duplicates ((Structures.SSet.elements set_var)@[init_var]) in
      let a = opt_apply top (List.map (fun (x : string) -> {SVAR.vname = x; vtyp = SVAR.Tnum}) list_var) in (* domains treat numbers, hence in the beginning, every ast var has type Tnum*)
      update a (Apron.Var.of_string init_var) v
  end

 

module Server_State = Server_Concrete_State(Language.Server_Language)(Language.Client_Language)(V.Concrete_Integer)(V.Concrete_Boolean)(Domain.PA_box)
module Server_State_F = Server_Concrete_State(Language.Server_Language)(Language.Client_Language)(V.Concrete_Float)(V.Concrete_Boolean)(Domain.PA_box)
module Server_State_Intv = Server_Concrete_State(Language.Server_Language)(Language.Client_Language)(V.Interval)(V.Concrete_Boolean)(Domain.PA_box)
module Server_AState_Box = Server_Abstract_State(Language.Server_Language)(Language.Client_Language)(V.Interval)(V.Concrete_Boolean)(Domain.PA_box)
module Server_AState_Oct = Server_Abstract_State(Language.Server_Language)(Language.Client_Language)(V.Interval)(V.Concrete_Boolean)(Domain.PA_oct)


  module type CLIENT_STATE  =
  sig
    module CVAL : Language.CLIENT_VALUE
    module NUM : V.NUMBERS
    module BOOL : V.BOOLEANS
    type t
    type key
    type value = | SNum of NUM.t | SBool of BOOL.t
    val key_of_string : string -> key
    val update : t -> key -> value -> t
    val find : t -> key -> value
    val to_avalue : CVAL.t -> value
    val new_state :  t
    val is_true : value -> bool
    val value_compare : value -> value -> int
    val to_string : value -> string
  end


  module Client_Concrete_State (CVAL : Language.CLIENT_VALUE) (NUM : V.NUMBERS) (BOOL : V.BOOLEANS) : CLIENT_STATE =
  struct
    module CVAL = CVAL
    module NUM = NUM
    module BOOL = BOOL

    type key = string
    type value = SNum of NUM.t | SBool of BOOL.t
    type t = value SMap.t
    let key_of_string (k : string) : key = k
    let update (m : t) (k : key) (v : value) : t =
      SMap.add k v m
    let find (m : t) (k : key) : value =
      SMap.find k m
    let to_avalue (sv : CVAL.t) : value =
      match sv with
      | VNum i -> SNum (NUM.of_int i)
      | VBool b  -> SBool (BOOL.of_bool b)
    let new_state : t = SMap.empty
    let is_true (v : value) : bool =
      match v with
      | SBool b -> BOOL.is_true b
      | _ -> false
    let value_compare v1 v2 =
      match (v1,v2) with
      | (SNum i1, SNum i2) -> NUM.compare i1 i2
      | (SBool i1, SBool i2) -> BOOL.compare i1 i2
      | (SNum _, SBool _) -> 1
      | _ -> -1
    let to_string (v : value) : string =
      match v with
      | SNum i -> "NUM "^(NUM.to_string i)
      | SBool b -> "BOOL "^(BOOL.to_string b)
  end

  module Client_State = Client_Concrete_State(Language.Client_Value)(V.Concrete_Integer(Domain.PA_box))(V.Concrete_Boolean)
  module Client_State_F = Client_Concrete_State(Language.Client_Value)(V.Concrete_Float(Domain.PA_box))(V.Concrete_Boolean)
  module Client_State_Intv = Client_Concrete_State(Language.Client_Value)(V.Interval(Domain.PA_box))(V.Concrete_Boolean)


