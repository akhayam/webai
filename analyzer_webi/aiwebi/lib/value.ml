
module A = Apron

module type BOOLEANS = sig
  type t
  val fls : t
  val tru : t
  val of_bool : bool -> t
  val (&&) : t -> t -> t
  val (||) : t -> t -> t
  val (!) : t -> t
  val is_true : t -> bool
  val compare : t -> t -> int
  val to_string : t -> string
  val mk_const : unit -> t
end

module Concrete_Boolean : BOOLEANS =
struct
 module BOOL = Bool
 include BOOL
 type t = BOOL.t
 let fls = false
 let tru = true
 let of_bool (b : bool) : t = b
 let (&&) = Stdlib.(&&)
 let (||) = Stdlib.(||)
 let (!) = BOOL.not
 let is_true (b : t) : bool = (b == true)
 let compare (b1 : t) (b2 : t) : int = BOOL.compare b1 b2
 let to_string (b : t) : string = BOOL.to_string b
 let mk_const () : t = Random.self_init (); Random.bool ()
end


module type NUMBERS = sig
  module PA : Domain.PRE_APRON
  type t 
  val of_int : int -> t
  val of_ints : int -> int -> t
  val  (+) : t -> t -> t  
  val  (-) : t -> t -> t  
  val  ( * ) : t -> t -> t  
  val  (<) : t -> t -> bool 
  val  (<=) : t -> t -> bool 
  val  (>) : t -> t -> bool 
  val  (>=) : t -> t -> bool 
  val  (==) : t -> t -> bool 
  val  (!=) : t -> t -> bool 
  val (!) : t -> t
  val (--) : t -> t -> t
  val compare : t -> t -> int
  val to_string : t -> string
  val is_top : (t -> bool) Option.t
  val is_bottom : (t -> bool) Option.t
  val bottom : (t Option.t*string)
  val mk_const : ?i0:int Option.t -> int -> t
  val to_coeff : t -> PA.atyp -> Apron.Coeff.t
end

module Concrete_Integer (PA : Domain.PRE_APRON) : NUMBERS =
struct
  module PA = PA
  module INT = Int
  include INT
  type t = INT.t
  let of_int (i : INT.t) : t = i
  (*let of_ints _ _ : t = failwith "Cannot create an interval from concrete integers"*)
  let (+) = INT.add
  let (-) = INT.sub
  let ( * ) = INT.mul
  let (>) (i1 : t) (i2 : t) : bool = (Stdlib.(>) i1 i2)
  let (<) (i1 : t) (i2 : t) : bool = (Stdlib.(<) i1 i2)
  let (>=) (i1 : t) (i2 : t) : bool = (Stdlib.(>=) i1 i2)
  let (<=) (i1 : t) (i2 : t) : bool = (Stdlib.(>=) i1 i2)
  let (==) (i1 : t) (i2 : t) : bool = (Stdlib.(==) i1 i2)
  let (!=) (i1 : t) (i2 : t) : bool = (Stdlib.(!=) i1 i2)
  let (!) (i : t) : t =  i * INT.minus_one
  let (--) : t -> t -> t = INT.sub
  let compare (i1 : t) (i2 : t) : int = INT.compare i1 i2
  let to_string (i : t) : string = "INT "^(INT.to_string i)
  let is_top : (t -> bool) Option.t = None
  let is_bottom : (t -> bool) Option.t = None
  let bottom : (t Option.t*string) = (None,"ERROR : Concrete Integers do not have bottom")
  let to_coeff (i : t) (typ : PA.atyp): Apron.Coeff.t = 
    match typ with
    | BOX -> Apron.Coeff.i_of_int i i
    | OCT -> Apron.Coeff.s_of_int i
    | _ -> failwith "not supporting other domains than BOX"
  let of_ints _ _ : t = failwith "ERROR : Cannot create an interval from concrete integer"
  let mk_const ?(i0 = None) (i : int) : t =
      match i0 with
      | None -> Random.self_init (); Random.int i
      | Some _ -> Random.self_init (); Random.int i
end

(*module Concrete_Mpz (PA : Domain.PRE_APRON): NUMBERS =
struct
  module PA = PA
  module NUM = Mpz
  include NUM
  type t = NUM.t
  let of_int (i : Int.t) : t = NUM.of_int i
  let (+) (i1 : t) (i2 : t) = NUM.add i1 i1 i2; i1
  let (-) (i1 : t) (i2 : t) = NUM.sub i1 i1 i2; i1
  let ( * ) (i1 : t) (i2 : t) = NUM.mul i1 i1 i2; i1
  let (>) (i1 : t) (i2 : t) : bool = (Stdlib.(>) i1 i2)
  let (<) (i1 : t) (i2 : t) : bool = (Stdlib.(<) i1 i2)
  let (>=) (i1 : t) (i2 : t) : bool = (Stdlib.(>=) i1 i2)
  let (<=) (i1 : t) (i2 : t) : bool = (Stdlib.(>=) i1 i2)
  let (==) (i1 : t) (i2 : t) : bool = (Stdlib.(==) i1 i2)
  let (!=) (i1 : t) (i2 : t) : bool = (Stdlib.(!=) i1 i2)
  let (!) (i : t) : t =  ( * ) i (NUM.of_int (Int.minus_one))
  let compare (i1 : t) (i2 : t) : int = NUM.cmp i1 i2
  let to_string (i : t) : string = "MPZ "^(NUM.to_string i)
  let is_top : (t -> bool) Option.t = None
  let is_bottom : (t -> bool) Option.t = None
  let to_coeff (i : t) (typ : PA.atyp): Apron.Coeff.t = 
    match typ with
    | BOX ->  Apron.Coeff.i_of_int (NUM.get_int i) (NUM.get_int i)
    | _ -> failwith "not supporting other domains than BOX"
  let of_ints _ _ : t = failwith "ERROR : Cannot create an interval from concrete MPZ"
  let mk_const (i : int) : t = Random.self_init (); NUM.of_int (Random.int i)
end
*)

module Concrete_Float (PA : Domain.PRE_APRON): NUMBERS =
struct
  module PA = PA
  module FL = Float
  include FL
  type t = FL.t
  let of_int (i : Int.t) : t = (FL.of_int i)
  let (+) = FL.add
  let (-) = FL.sub
  let (--) : t -> t -> t = FL.sub
  let ( * ) = FL.mul
  let (>) (i1 : t) (i2 : t) : bool = (Stdlib.(>) i1 i2)
  let (<) (i1 : t) (i2 : t) : bool = (Stdlib.(<) i1 i2)
  let (>=) (i1 : t) (i2 : t) : bool = (Stdlib.(>=) i1 i2)
  let (<=) (i1 : t) (i2 : t) : bool = (Stdlib.(>=) i1 i2)
  let (==) (i1 : t) (i2 : t) : bool = (Stdlib.(==) i1 i2)
  let (!=) (i1 : t) (i2 : t) : bool = (Stdlib.(!=) i1 i2)
  let (!) (i : t) : t =  i * FL.minus_one
  let compare (i1 : t) (i2 : t) : int = FL.compare i1 i2
  let to_string (i : t) : string = "FLOAT "^(FL.to_string i)
  let is_top : (t -> bool) Option.t = None
  let is_bottom : (t -> bool) Option.t = None
  let bottom : (t Option.t*string) = (None,"ERROR : Concrete Float do not have bottom")
  let mk_const ?(i0 = None) (i : int) : t =
    match i0 with
    | None -> Random.self_init (); (Random.float (Float.of_int i))
    | Some _ -> Random.self_init (); (Random.float (Float.of_int i))

  let to_coeff (f : t) (typ : PA.atyp): Apron.Coeff.t = 
    match typ with
    | BOX -> Apron.Coeff.i_of_float f f
    | _ -> failwith "not supporting other domains than BOX"
  let of_ints _ _ : t = failwith "Cannot create an interval from concrete floats"
end

module Interval (PA : Domain.PRE_APRON): NUMBERS =
  struct
    module PA = PA
    module AI = A.Interval
    module AE = A.Texpr0
    module AS = A.Scalar
    module AC = A.Coeff
    module AU = Ailanguage_utils
    type t = AI.t
    let of_int (i : int) : t = (AI.of_int i i)
    let compare (ai1 : t) (ai2 : t): int =
      AI.cmp ai1 ai2
    let to_string (ai : t) : string =
      "INTERVAL ["^(AS.to_string ai.inf)^", "^(AS.to_string ai.sup)^"]"
    let arith_bop (op : A.Texpr0.binop)(s1 : AS.t) (s2 : AS.t) : AS.t =
      match op,s1,s2 with
      | Add, Float f1, Float f2 -> Float (f1 +. f2)
      | Sub, Float f1, Float f2 -> Float (f1 -. f2)
      | Mul, Float f1, Float f2 -> Float (f1 *. f2)
      | Add, Mpqf m1, Mpqf m2 -> Mpqf (Mpqf.add m1 m2)
      | Sub, Mpqf m1, Mpqf m2 -> Mpqf (Mpqf.sub m1 m2)
      | Mul, Mpqf m1, Mpqf m2 -> Mpqf (Mpqf.mul m1 m2)
      | Add, Mpfrf m1, Mpfrf m2 -> Mpfrf (Mpfrf.add m1 m2 Near)
      | Sub, Mpfrf m1, Mpfrf m2 -> Mpfrf (Mpfrf.sub m1 m2 Near)
      | Mul, Mpfrf m1, Mpfrf m2 -> Mpfrf (Mpfrf.mul m1 m2 Near)
      | op, o1, o2  -> Printf.printf "operation %s\n" (AU.apron_arith_bop_2str op);
                       Printf.printf "operands : o1 =  %s , o2 = %s"
                        (match o1 with
                         | Float f -> "float"^" "^(string_of_float f)
                         | Mpqf m -> "mpqf "^(Mpqf.to_string m)
                         | Mpfrf _ -> "mpfrf") (match o2 with
                         | Float f -> "float"^" "^(string_of_float f)
                         | Mpqf _ -> "mpqf"
                         | Mpfrf _ -> "mpfrf");                       
                       failwith "ERROR: other binary interval operations not implemented"
    let arith_uop (op : A.Texpr0.unop) (s : AS.t) : AS.t =
      match op,s with
      | Neg, Float f -> Float (f *. (-1.))
      | Neg, Mpqf m -> Mpqf (Mpqf.neg m)
      | Neg, Mpfrf m -> Mpfrf (Mpfrf.neg m Near)
      | _ -> failwith "ERROR : other unary interval operations not implemented"
    let (+) (ai1 : t) (ai2 : t) : t =
      {inf = arith_bop Add ai1.inf ai2.inf; sup = arith_bop Add ai1.sup ai2.sup}
    let (-) (ai1 : t) (ai2 : t) : t =
    {inf = arith_bop Sub ai1.inf ai2.sup; sup = arith_bop Sub ai1.sup ai2.inf}
    let (--) (ai1 : t) (ai2 : t) : t =
    {inf = arith_bop Sub ai1.inf ai2.sup; sup = arith_bop Sub ai1.sup ai2.sup}
    let ( * ) (ai1 : t) (ai2 : t) : t =
    {inf = arith_bop Mul ai1.inf ai2.inf; sup = arith_bop Mul ai1.sup ai2.sup}
    let (>) (i1 : t) (i2 : t) : bool = (AI.is_leq i2 i1) && Bool.not (AI.equal i1 i2)
    let (<) (i1 : t) (i2 : t) : bool =  (AI.is_leq i1 i2) && Bool.not (AI.equal i1 i2)
    let (>=) (i1 : t) (i2 : t) : bool =  AI.is_leq i2 i1
    let (<=) (i1 : t) (i2 : t) : bool =  AI.is_leq i1 i2
    let (==) (i1 : t) (i2 : t) : bool =  AI.equal i1 i2
    let (!=) (i1 : t) (i2 : t) : bool =  Bool.not (AI.equal i1 i2)
    let (!) (ai : t) : t =
    {inf = arith_uop Neg ai.sup ; sup = arith_uop Neg ai.inf}
    let is_top : (t -> bool) Option.t = Some (AI.is_top) 
    let is_bottom : (t -> bool) Option.t = Some (AI.is_bottom)
    let bottom : (t Option.t*string) = (Some (AI.bottom),"")
    let mk_const ?(i0 = None)(i : int) : t = 
      match i0 with
      | None -> AI.of_int i i
      | Some i0 -> AI.of_int i0 i
    let to_coeff (i : t) (typ : PA.atyp): Apron.Coeff.t = 
      match typ with
      | BOX -> Interval i
      | OCT -> Interval i
      | _ -> failwith "not supporting other domains than BOX"
    let of_ints i1 i2 : t = AI.of_int i1 i2
    
  end
