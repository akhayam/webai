module type CLIENT_VARIABLE =
  sig
  type t = { vname : vname; vtyp : typ}
  and vname = string
  and typ = | Tnum | Tbool | Tunk
  val make : vname -> typ -> t
  val get_vname : t -> vname
  val get_vtype : t -> typ
  val string_of_vname : vname -> string
  val compare : t -> t -> int
  (*module VOrd = struct type t = var let compare = compare end
  module VMap = Map.Make(VOrd)
  module VSet = Set.Make(VOrd)*)
  val pp : out_channel -> t -> unit
  val to_string : typ -> string    
  end


module Client_Variable : CLIENT_VARIABLE =
  struct
    type t = { vname : vname; vtyp : typ}
    and vname = string
    and typ = | Tnum | Tbool | Tunk
    type var = t
    let make (s : vname) (t : typ)  = {vname = s; vtyp = t}
    let get_vname v = v.vname
    let get_vtype v = v.vtyp
    let string_of_vname (v : vname) : string = v
    let compare v0 v1 = String.compare (get_vname v0) (get_vname v1)
    module VOrd = struct type t = var let compare = compare end
    module VMap = Map.Make(VOrd)
    module VSet = Set.Make(VOrd)
    let pp chan v = Printf.fprintf chan "%s" v.vname
    let to_string = function Tnum -> "num" | Tbool -> "bool" | Tunk -> "unknown"
  end

  module type SERVER_VARIABLE =
    sig
      type t = { vname : vname; vtyp : typ}
      and vname = string
      and typ = | Tnum | Tbool | TClos | Tunk
      val make : vname -> typ -> t
      val get_vname : t -> vname
      val get_vtype : t -> typ
      val string_of_vname : vname -> string
      val vname_of_string : string -> vname
      val compare : t -> t -> int
      (*module VOrd = struct type t = var let compare = compare end
      module VMap = Map.Make(VOrd)
      module VSet = Set.Make(VOrd)*)
      val pp : out_channel -> t -> unit
      val to_string : typ -> string    
    end

module Server_Variable : SERVER_VARIABLE =
  struct
    type t = { vname : vname; vtyp : typ}
    and vname = string
    and typ = | Tnum | Tbool | TClos | Tunk
    type var = t
    let make (s : vname) (t : typ)  = {vname = s; vtyp = t}
    let get_vname v = v.vname
    let get_vtype v = v.vtyp
    let string_of_vname (v : vname) : string = v
    let vname_of_string (s : string) : vname = s
    let compare v0 v1 = String.compare (get_vname v0) (get_vname v1)
    module VOrd = struct type t = var let compare = compare end
    module VMap = Map.Make(VOrd)
    module VSet = Set.Make(VOrd)
    let pp chan v = Printf.fprintf chan "%s" v.vname
    let to_string = function Tnum -> "num" | Tbool -> "bool" | TClos -> "closure" | Tunk -> "unknown"
  end
