open Apron

module Op = Op.Op
module S = Language.Server_Language

(* #### Apron Base types cast ##### *)
let coeff_2str  : Coeff.t -> string = function
  | Coeff.Scalar s -> Scalar.to_string s
  | Coeff.Interval _ -> failwith "pp_coeff-interval"

let coeff_2int : Coeff.t -> int = function
  | Coeff.Scalar s ->
    int_of_float
      (match s with
      | Float f -> f
      | Mpqf f -> Mpqf.to_float f
      | Mpfrf f -> Mpfrf.to_float f)
  | Coeff.Interval _ -> failwith "pp_coeff-interval"

(*#### Apron Ops to Ops ####*)
let apron_uop_2uop : Texpr0.unop -> Op.uop = function
| Texpr0.Neg -> Op.Neg
| _ -> failwith "ERROR : Texpr0 unop not supported"

let apron_arith_bop_2bop : Texpr0.binop -> Op.bop = function 
  | Texpr0.Add -> Op.Add
  | Texpr0.Sub -> Op.Sub
  | Texpr0.Mul -> Op.Mul
  | _ -> failwith "ERROR : binop unsopported by the language"

let apron_log_bop_2bop : Lincons1.typ -> Op.bop = function
  | Lincons1.EQ -> Op.Eq
  | Lincons1.DISEQ -> Op.NEq
  | Lincons1.SUP -> Op.GT
  | Lincons1.SUPEQ -> Op.GEq
  | _ -> failwith "ERROR : Lincons1 unsopported by the language"


(*#### PRINTERS ####*)
let apron_uop_2str : Texpr0.unop -> string = function
  | Texpr0.Neg -> "!"
  | _ -> failwith "ERROR : Texpr0 unop not supported"

let apron_arith_bop_2str : Texpr0.binop -> string = function
  | Texpr0.Add -> "+"
  | Texpr0.Sub -> "-"
  | Texpr0.Mul -> "*"
  | _ -> failwith "ERROR : Texpt0 binop not supported"

let apron_log_bop_2str : Lincons1.typ -> string = function
| Lincons1.EQ -> "="
| Lincons1.DISEQ -> "!="
| Lincons1.SUP -> ">"
| Lincons1.SUPEQ -> ">="
| _ -> failwith "ERROR : Lincons1 unsopported in logop_2str"

(* Apron level 1 Expressions to strings *)
let apron_arith_expr_2str (te: Texpr1.t): string =
  let rec aux_expr (e: Texpr1.expr): string =
    match e with
    | Texpr1.Cst c -> coeff_2str c
    | Texpr1.Var v -> Var.to_string v
    | Texpr1.Unop (u, te0, _, _) ->
        Printf.sprintf "%s (%s)" (apron_uop_2str u) (aux_expr te0)
    | Texpr1.Binop (b, te0, te1, _, _) ->
        Printf.sprintf "(%s) %s (%s)"
          (aux_expr te0) (apron_arith_bop_2str b) (aux_expr te1) in
  aux_expr (Texpr1.to_expr te)

(* Debug pp for environments *)
  let environment_2str (env: Environment.t): string =
    let ai, af = Environment.vars env in
    (* Check no Real vars *)
    assert (Array.length af = 0);
    let isbeg = ref true in
    let s =
      Array.fold_left
        (fun (acc: string) (v: Var.t) ->
          let sep =
            if !isbeg then
              begin
                isbeg := false;
                ""
              end
            else "; " in
          Printf.sprintf "%s%s%s" acc sep (Var.to_string v)
        ) "" ai in
    Printf.sprintf "[|%s|]" s


(* Extraction of integer variables *)
let get_ivars (env: Environment.t): Var.t array =
  let ivars, fvars = Environment.vars env in
  if Array.length fvars > 0 then failwith "unimp: floating-point vars";
  ivars

(* Determines if set of constraints is SAT *)
  let linconsarray_sat (a: Lincons1.earray): bool =
    let f_lincons (cons: Lincons1.t) =
      if Lincons1.is_unsat cons then false
      else true in
    (* Array of cons1 *)
    let ac1 =
      Array.mapi
        (fun i _ -> Lincons1.array_get a i)
        a.Lincons1.lincons0_array in
    Array.fold_left
      (fun (acc: bool) cons -> acc && (f_lincons cons)) true ac1
      
  (* Turns a set of constraints into a string *)
  let linconsarray_2stri (ind: string) (a: Lincons1.earray): string =
    (* Extraction of the integer variables *)
    let env = a.Lincons1.array_env in
    let ivars = get_ivars env in
    (* pretty-printing of a constraint *)
    let f_lincons (cons: Lincons1.t): string =
      if Lincons1.is_unsat cons then Printf.sprintf "%sUNSAT lincons\n" ind
      else
        (* print non zero coefficients *)
        let mt = ref false in
        let str0 =
          Array.fold_left
            (fun str v ->
              let c =
                try Lincons1.get_coeff cons v
                with exn ->
                  failwith (Printf.sprintf "get_coeff, var %s, exn %s"
                              (Var.to_string v) (Printexc.to_string exn));
              in
              if not (Coeff.is_zero c) then
                begin
                  let var = Var.to_string v in
                  let str_ext = Printf.sprintf "%s . %s" (coeff_2str c) var in
                  if !mt then
                    Printf.sprintf "%s + %s" str str_ext
                  else
                    begin
                      mt := true;
                      str_ext
                    end
                end
              else str
            ) "" ivars in
        (* print the constant *)
        let str1 =
          let d0 = coeff_2str (Lincons1.get_cst cons) in
          if !mt then Printf.sprintf "%s + %s" str0 d0
          else d0 in
        (* print the relation *)
        Printf.sprintf "%s%s %s\n" ind str1
          (apron_log_bop_2str (Lincons1.get_typ cons)) in
    (* Array of cons1 *)
    let ac1 =
      Array.mapi
        (fun i _ -> Lincons1.array_get a i)
        a.Lincons1.lincons0_array in
    Array.fold_left
      (fun (acc: string) cons ->
        Printf.sprintf "%s%s" acc (f_lincons cons)
      ) "" ac1
  
  let lincons_2expr (cons: Lincons1.t) : S.expr =
    if Lincons1.is_unsat cons then
      begin
        Printf.printf "\nUNSAT lincons";
        Value (VBool false)
      end
    else
      let env = Lincons1.get_env cons in
      let ivars = get_ivars env in
      let cst : S.expr = Value (VNum (coeff_2int (Lincons1.get_cst cons))) in
      let linexpr = Array.fold_left
        (fun expr v : S.expr ->
          let c =
            try coeff_2int (Lincons1.get_coeff cons v)
            with exn ->
              failwith (Printf.sprintf "get_coeff, var %s, exn %s"
                (Var.to_string v) (Printexc.to_string exn));
          in
          let v = S.SVAR.make (Var.to_string v) S.SVAR.Tnum in
          let m : S.expr = match c with
          | 0 -> Value (VNum  0)
          | 1 -> Var v.vname
          | _ -> BOp (Op.Mul, Value (VNum c), Var v.vname) in
          match m with
          | Value (VNum 0) -> expr
          | _ -> BOp (Op.Add, m, expr))
        cst ivars
      in
      let op = apron_log_bop_2bop (Lincons1.get_typ cons) in
      BOp (op, linexpr, Value (VNum 0))
  
  (* Translation into Apron expressions, with environment *)
  let translate_op : Op.bop -> Texpr0.binop = function
    | Add -> Texpr1.Add
    | Sub -> Texpr1.Sub
    | Mul -> Texpr1.Mul
    | _    -> failwith "binary operator, unsupported in translate_op"
  let translate_cond : Op.bop -> Lincons0.typ = function
    | Eq  -> Tcons1.EQ
    | NEq  -> Tcons1.DISEQ
    | LT  -> Tcons1.SUP
    | LEq  -> Tcons1.SUPEQ
    | _    -> failwith "condition, unsupported in translate_cond"
  let rec translate_expr (env: Environment.t) : S.expr -> Texpr1.t = function
    | Value (VNum i) -> Texpr1.cst env (Coeff.s_of_int i)
    | Value _ -> failwith "boolean constant, unsupported in translate_expr"
    | Var  v -> Texpr1.var env (Var.of_string v)
    | UOp  _ -> failwith "negation, unsupported in translate_expr"
    | BOp (b,e0,e1) ->
        Texpr1.binop
          (translate_op b)
          (translate_expr env e0)
          (translate_expr env e1) Texpr1.Int Texpr1.Near
    | Tilde _ -> failwith "unsopported tilde expression in translate_expr"
  let translate_cons (env: Environment.t) : S.expr -> Tcons1.t = function
    | Value (VBool b) ->
        if b then (* tautology constraint 0 = 0 *)
          let ex = translate_expr env (Value (VNum 0)) in
          Tcons1.make ex Tcons1.EQ
        else (* anti-tautology constraint 1 = 0 *)
          let ex = translate_expr env (Value (VNum 1)) in
          Tcons1.make ex Tcons1.EQ
    | BOp (Eq, e0, e1) ->
        Tcons1.make (translate_expr env (BOp (Sub,e1,e0))) Tcons1.EQ
    | BOp (NEq,e0,e1) ->
        Tcons1.make (translate_expr env (BOp (Sub,e1, e0))) Tcons1.DISEQ
    | BOp (LEq, e0, e1) ->
        Tcons1.make (translate_expr env (BOp (Sub,e1, e0))) Tcons1.SUPEQ
    | BOp (LT,e0, e1) ->
        Tcons1.make (translate_expr env (BOp (Sub,e1, e0))) Tcons1.SUP
    | BOp (GEq,e0, e1) ->
        Tcons1.make (translate_expr env (BOp (Sub,e0, e1))) Tcons1.SUPEQ
    | BOp (GT,e0, e1) ->
        Tcons1.make (translate_expr env (BOp (Sub,e0, e1))) Tcons1.SUP
    | _ -> failwith "condition, unsupported"






