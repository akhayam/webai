
module SSet = Set.Make(String)

module IMap = Map.Make(Int)

module IIMap = Map.Make(
  struct
    type t = (int * int)    
    let compare (n1,a1) (n2,a2) =
      let c = Int.compare n1 n2 in
      match c = 0 with
      | true -> Int.compare a1 a2
      | false -> c
  end)

module CodeMap = Map.Make(
  struct
    type t = int * int
    let compare (i1,i2) (i3,i4) =
      let c = Int.compare i1 i3 in
      (match c with
      | 0 -> Int.compare i2 i4
      | _ -> c)
  end

)

module SMap = Map.Make(String)
module SSMap = Map.Make(
  struct
    type t = (string * string)    
    let compare (n1,a1) (n2,a2) =
      let c = String.compare n1 n2 in
      match c = 0 with
      | true -> String.compare a1 a2
      | false -> c
  end)
