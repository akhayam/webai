module S = State
module O = Op.Op
module SMap = Structures.SMap
module W = Web_utils.Web
module V = Value

let (let*) ((w,s) : ('a Option.t*string))(f : 'a -> 'b): 'b =
  match w with
  | Some v -> f v
  | None -> failwith s
let (let&) ((w,s) : ('a Option.t*string)) (f : 'a -> 'a): 'a =
  match w with
  | Some v -> f v
  | None -> failwith s
let opt_apply (fo: (('a -> 'b) Option.t * string)) (p : 'a): 'b =
  (let* f = fo in f p)
let opt_extr (w: ('a Option.t * string)): 'a = (let& v = w in v)
let (let^) (l : 'a list) (f : 'a -> 'b) : 'b =
  match l with
  | [] -> failwith "Error : let^ expects one element in the list"
  | [x] -> f x
  | _::_ -> failwith "Error : let^ expects one element in the list"

module type SERVER_INTERPRETER =
  sig
    module ST : S.SERVER_STATE
    module CL := ST.CLAN
    module SL := ST.SLAN
    (*type direction = | Left | Right | Both | None*)
    type status =
      | Done of SL.ret
      | Actuating of (SL.DL.actuator * SL.DL.id)
      | Reading of (SL.SVAR.vname * SL.DL.id)
      | Ret of (SL.SVAR.vname * SL.SVAL.t)
      | Running
      (*| Split of (direction * configuration * configuration)*)
    and configuration = {
      prg : SL.block;
      pp : int;
      status : status;
      state : ST.t ;
      expr : SL.expr list
    }
    val to_state_sval : SL.SVAL.t -> ST.SLAN.SVAL.t
    val eval : configuration -> configuration List.t
  end

module Server_Concrete_Interpreter (ST : S.SERVER_STATE) : SERVER_INTERPRETER =
  struct
  module ST = ST
  module SL = ST.SLAN
  module CL = ST.CLAN

  (*type direction = | Left | Right | Both | None*)
  type status =
    | Done of SL.ret
    | Actuating of (SL.DL.actuator * SL.DL.id)
    | Reading of (SL.SVAR.vname * SL.DL.id)
    | Ret of (SL.SVAR.vname * SL.SVAL.t)
    | Running
    (*| Split of (direction * configuration * configuration)*)
  and configuration = {
    prg : SL.block;
    pp : int;
    status : status;
    state : ST.t;
    expr : SL.expr list}
  let do_uop (o : O.uop) (v : ST.value) =
    match (o,v) with
    | (Neg,SNum i) -> ST.SNum (ST.NUM.(!) i)
    | (Neg,SBool b) -> ST.SBool (ST.BOOL.(!) b)
    | _ -> failwith "ERROR : Not possible to do a unary operation on closures"  
  let do_bop (o : O.bop) (v1 : ST.value) (v2 : ST.value) : ST.value =
    match (o,v1,v2) with
    | (Add,SNum i1,SNum i2) -> SNum (ST.NUM.(+) i1 i2)
    | (Sub,SNum i1,SNum i2) -> SNum (ST.NUM.(-) i1 i2)
    | (Mul,SNum i1,SNum i2) -> SNum (ST.NUM.( * ) i1 i2)
    | (LT,SNum i1,SNum i2) -> SBool (ST.BOOL.of_bool (ST.NUM.(<) i1 i2))
    | (LEq,SNum i1,SNum i2) -> SBool (ST.BOOL.of_bool (ST.NUM.(<=) i1 i2))
    | (GT,SNum i1,SNum i2) -> SBool (ST.BOOL.of_bool (ST.NUM.(>) i1 i2))
    | (GEq,SNum i1,SNum i2) -> SBool (ST.BOOL.of_bool (ST.NUM.(>=) i1 i2))
    | (Eq,SNum i1,SNum i2) -> SBool (ST.BOOL.of_bool (ST.NUM.(==) i1 i2))
    | (NEq,SNum i1,SNum i2) -> SBool (ST.BOOL.of_bool (ST.NUM.(!=) i1 i2))
    | (And,SBool b1,SBool b2) -> SBool (ST.BOOL.(&&) b1 b2)
    | (Or,SBool b1,SBool b2) -> SBool (ST.BOOL.(||) b1 b2)
    | _ -> failwith "ERROR : Mixed binary operation or closure"

  let to_state_sval (v : SL.SVAL.t) : ST.SLAN.SVAL.t =
    match v with
    | VNum i -> ST.SLAN.SVAL.VNum i
    | VBool b -> ST.SLAN.SVAL.VBool b
    | _ -> failwith "ERROR : to_state_value does not support closures"

  let rec eval_expr (e : SL.expr) (s : ST.t) : ST.value=
    (match e with
    | Value v -> ST.to_state_value (to_state_sval v)
    | Var x -> (ST.find s (ST.key_of_string x))
    | BOp (o,e1,e2) ->
        let v1 = eval_expr e1 s in
        let v2 = eval_expr e2 s in
        do_bop o v1 v2
    | UOp (o,e') ->
        let v = eval_expr e' s in
        do_uop o v
    | _ -> failwith "ERROR : Not handling yet tilda expressions")
  
    let rec eval (c : configuration) : configuration List.t =
      match c.prg with
      | [] -> [{c with status = Done (V (SL.SVAL.VBool false))}] (*returning a value instead of code*)
      | x::t ->
        (match x with
        | Ass (x,e) ->
          let v = eval_expr e c.state in
          eval {c with (*pp = (SL.get_first_line t);*)
                       prg = t;
                       state = (ST.update c.state (ST.key_of_string x) v)}
        | Skip -> 
          eval {c with (*pp = SL.get_first_line t;*)
                       prg = t}
        | Return _ ->
               [{c with status = Done (V (SL.SVAL.VBool false));
                        (*pp = SL.get_first_line t;*)
                        prg = []}]
        |  If (e,b1,b2) ->
            let b = eval_expr e c.state in
            let c = 
            (match ST.is_true b with
            | true -> eval {c with prg = b1@t;
                                         (*pp = SL.get_first_line b1;*)
                                         expr = c.expr@[e]}
            | false -> eval {c with prg = b2@t;
                                          (*pp = SL.get_first_line b2;*)
                                          expr = c.expr@[UOp (Neg, e)]}) in
            c
        | While (e,b) ->
            let v = eval_expr e c.state in
            let c = 
              (match ST.is_true v with
              | true -> eval {c with prg = (b@[x]@t);
                                     expr = c.expr@[e]}
              | false -> eval {c with prg = t;
                                      expr = c.expr@[UOp (Neg, e)]}) in c
        | Act (a,d,_) -> [{c with prg = t; status = Actuating (a,d)}]
        | Get (x,d,_) -> [{c with prg = [SL.Wait x]@t; status = Reading (x,d)}]
        | Wait _ -> [{c with prg = t; status = Running}])
  end

  module Server_Abstract_Interpreter (ST : S.SERVER_STATE) : SERVER_INTERPRETER=
  struct
    module ST = ST
    module SL = ST.SLAN
    module CL = ST.CLAN
    module A = Apron.Abstract1
    module AIu = Ailanguage_utils
    module Op = Op.Op


    (*type direction = | Left | Right | Both | None*)
    type status =
    | Done of SL.ret
    | Actuating of (SL.DL.actuator * SL.DL.id)
    | Reading of (SL.SVAR.vname * SL.DL.id)
    | Ret of (SL.SVAR.vname * SL.SVAL.t)
    | Running
    (*| Split of (direction * configuration * configuration)*)
    and configuration = {
      prg : SL.block; (*in case of split, the block must be the continuation*)
      pp : int;
      status : status;
      state : ST.t;
      expr : SL.expr list}

(* #### Apron Base types cast ##### *)
    let to_state_sval (v : SL.SVAL.t) : ST.SLAN.SVAL.t =
      match v with
      | VNum i -> ST.SLAN.SVAL.VNum i
      | VBool b -> ST.SLAN.SVAL.VBool b
      | _ -> failwith "ERROR : to_state_value does not support closures"

    let rec eval (c : configuration) : configuration List.t =
        match c.prg with
        | [] -> 
            [{c with status = Done (V (SL.SVAL.VBool false))}] (*returning a value instead of code*)
        | x::t ->
          (match x with
          | Ass (x,e) ->
            eval {c with (*pp = (SL.get_first_line t);*)
                        prg = t;
                        state = ( opt_apply ST.post_assign {vname = x; vtyp = Tnum} e c.state)}
          | Skip -> 
            eval {c with (*pp = SL.get_first_line t;*)
                        prg = t}
          | If (e,b1,b2) ->
              let apreT = opt_apply ST.post_cond e c.state in
              let apreF = opt_apply ST.post_cond (SL.expr_neg e) c.state in
              (* Analysis of both branches. They possibly stop executing in
                case at least one of the branches interacts with devices *)
              (match opt_apply ST.is_bottom apreT, opt_apply ST.is_bottom apreF with
              | true, true -> failwith "ERROR : one of the variables involved is bottom, otherwise not possible"
              | false, true ->
                eval {c with (*pp = SL.get_first_line b1;*)
                             prg = b1@t;
                             state = apreT}
              | true, false ->
                eval {c with (*pp = SL.get_first_line b2;*)
                             prg = b2@t;
                             state = apreF}
              | false,false ->
                (eval {c with (*pp = SL.get_first_line b1;*)
                                  prg = b1;
                                  state = apreT})@
                (eval {c with (*pp = SL.get_first_line b2;*)
                                    prg = b2;
                                    state = apreF}))
          | While (e,b) -> eval {c with prg = [SL.If (e,b,[SL.Skip])]@t}   
          | Act (a,d,_) -> 
              [{c with prg = t;(*pp = SL.get_first_line t;*) status = Actuating (a,d)}]
          | Get (x,d,_) -> 
            [{c with prg = [SL.Wait x]@t; pp = c.pp; status = Reading (x,d)}]
          | Wait _ -> 
            [{c with prg = t; (*pp = SL.get_first_line t;*) status = Running}]
          | _ -> failwith "ERROR : to do")
end

  module Server_Interpreter = Server_Concrete_Interpreter(S.Server_State)
  module Server_Interpreter_F = Server_Concrete_Interpreter(S.Server_State_F)
  module Server_Interpreter_Intv = Server_Concrete_Interpreter(S.Server_State_Intv)
  module AServer_interpreter = Server_Abstract_Interpreter(S.Server_AState_Box)
  module AServer_interpreter_Oct = Server_Abstract_Interpreter(S.Server_AState_Oct)
  
  module type CLIENT_INTERPRETER =
  sig
    module ST : S.CLIENT_STATE
    module CL : Language.CLIENT_LANGUAGE

    type configuration = {
      origin_url : W.url;
      prg : CL.client_exec_type;
      pp : int;
      status : CL.status;
      state : ST.t;
    }
    val eval : configuration -> configuration
  end

module Client_Concrete_Interpreter (ST : S.CLIENT_STATE) (CL : Language.CLIENT_LANGUAGE) : CLIENT_INTERPRETER =
  struct
  module CL = CL
  module ST = ST
  type configuration = {
    origin_url : W.url;
    prg : CL.client_exec_type;
    pp : int;
    status : CL.status;
    state : ST.t;
    }
    let eval (c : configuration) : configuration = c
  end

module Client_Interpreter = Client_Concrete_Interpreter(S.Client_State)(Language.Client_Language)
module Client_Interpreter_F = Client_Concrete_Interpreter(S.Client_State_F)(Language.Client_Language)
(*module Client_Interpreter_Mpz = Client_Concrete_Interpreter(S.Client_State_Mpz)(Language.Client_Language)*)
module Client_Interpreter_Intv = Client_Concrete_Interpreter(S.Client_State_Intv)(Language.Client_Language)
