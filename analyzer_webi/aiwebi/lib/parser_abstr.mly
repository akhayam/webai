%{

    module WI = Webi.Webi_AInterpreter
    module SL = WI.WO.SI.ST.SLAN
    module CL = WI.WO.SI.ST.CLAN
    module Op = Op.Op
    module DST = WI.WO.SI.ST
  
%}

%token <string> IDENT
%token <int> NUM
%token <string> STR
%token CLIENT SERVICE NOTIMPLEMENTED DEVICE (*EMPTY*) CALLS
%token EQ LEQ GEQ AND OR NOT LT GT NEQ ARROW
%token PLUS MINUS TIMES
%token TILDE (*DOLLAR*)
%token TBOOL TNUM
%token WHILE IF (*FI*) ELSE SKIP ACT GET (*CALL*)
%token TRUE FALSE
%token SEMICOLON LPAR RPAR LPB RPB COMMA ASS SRVS
%token EOF

%type <Webi.Webi_AInterpreter.program> webis
%start webis

%%

webis:
(*| EOF                                                                                           {[]}*)
| webi EOF                                                                                      {[$1]}
| webi webis                                                                                    {[$1]@($2)}
webi:
| SERVICE IDENT SRVS IDENT LPAR IDENT RPAR ASS sr_stmts                                         {WI.Service ($2,$4,$6,$9)}
| CLIENT NUM CALLS LPAR IDENT RPAR ASS cl_val_type                                              {WI.Client ($5,$8,$2)}
| DEVICE IDENT IDENT dv_val LPAR dev_sems RPAR                                                  {WI.Device (($2,$3),$4,$6)}
| DEVICE IDENT IDENT dv_val LPAR RPAR                                                           {WI.Device (($2,$3),$4,[])}
dev_sems:
| dev_sem                                                                                       {[$1]}
| dev_sem dev_sems                                                                              {[$1]@($2)}
dev_sem:
| LPB IDENT ARROW NUM RPB                                                                       {$2,DST.SNum (DST.NUM.of_int $4)}
| LPB IDENT ARROW TRUE RPB                                                                      {$2,DST.SBool DST.BOOL.tru}
| LPB IDENT ARROW FALSE RPB                                                                     {$2,DST.SBool DST.BOOL.fls}
cl_val_type:
| TNUM                                                                                          {CL.CVAR.Tnum}
| TBOOL                                                                                         {CL.CVAR.Tbool}
sr_stmts:
| sr_stmt SEMICOLON                                                                             {[$1]}
| sr_stmt SEMICOLON sr_stmts                                                                    {[$1]@($3)}
sr_stmt:
| IDENT ASS sr_expr                                                                             {SL.Ass ($1,$3)}
| IF LPAR sr_expr RPAR LPB sr_stmts RPB ELSE LPB sr_stmts RPB                                   {SL.If ($3,$6,$10)}
| WHILE LPAR sr_expr RPAR LPB sr_stmts RPB                                                      {SL.While ($3,$6)}
| SKIP                                                                                          {SL.Skip}
(*| RET sr_expr                                                                                   {SL.Ret $2}*)
| ACT LPAR IDENT COMMA LPAR IDENT COMMA IDENT RPAR COMMA STR RPAR                               {SL.Act ($3,($6,$8),())}
| GET LPAR IDENT COMMA LPAR IDENT COMMA IDENT RPAR COMMA STR RPAR                               {SL.Get ($3,($6,$8),())}
sr_expr:
| sr_val                                                                                        {SL.Value $1}
| IDENT                                                                                         {SL.Var $1}
| sr_expr binop sr_expr                                                                         {SL.BOp ($2,$1,$3)}
| LPAR sr_expr RPAR binop LPAR sr_expr RPAR                                                     {SL.BOp ($4,$2,$6)}
| unop sr_expr                                                                                  {SL.UOp ($1,$2)}
| TILDE LPAR tilde_lan RPAR                                                                     {SL.Tilde $3}
tilde_lan:
| NOTIMPLEMENTED                                                                                {failwith "ERROR : tilde language not implemented"}
sr_val:
| TRUE                                                                                          {SL.SVAL.VBool true}
| FALSE                                                                                         {SL.SVAL.VBool false}
| NUM                                                                                           {SL.SVAL.VNum $1}
dv_val:
| TRUE                                                                                          {DST.SBool DST.BOOL.tru}
| FALSE                                                                                         {DST.SBool DST.BOOL.fls}
| NUM                                                                                           {DST.SNum (DST.NUM.of_int $1)}
(*cl_stmts:
| cl_stmt SEMICOLON                                                                             {[$1]}
| cl_stmt SEMICOLON cl_stmts                                                                    {[$1]@($3)}
cl_stmt:
| IDENT ASS cl_expr                                                                             {CL.ASS ($1,$3)}
| IF LPAR cl_expr RPAR THEN cl_stmts ELSE cl_stmts FI                                           {CL.If ($3,$6,$8)}
| WHILE LPAR cl_expr RPAR LPB cl_stmts RPB                                                      {CL.While ($3,$6)}
| SKIP                                                                                          {CL.Skip}
(*| RET cl_expr                                                                                   {CL.Ret $2}*)
| CALL LPAR IDENT COMMA cl_val COMMA LPAR IDENT COMMA cl_stmt RPAR RPAR                         {SL.CL.Call ($3,$5,($8,$10))}
cl_expr:
| cl_val                                                                                        {CL.Value $1}
| IDENT                                                                                         {CL.Var $1}
| cl_expr binop cl_expr                                                                         {CL.BOp ($2,$1,$3)}
| unop cl_expr                                                                                  {SL.UOp ($1,$2)}*)
binop:
| PLUS                                                                                          {Add}
| MINUS                                                                                         {Op.Sub}
| TIMES                                                                                         {Op.Mul}
| LEQ                                                                                           {Op.LEq}
| GEQ                                                                                           {Op.GEq}
| LT                                                                                            {Op.LT}
| GT                                                                                            {Op.GT}
| NEQ                                                                                           {Op.NEq}
| EQ                                                                                            {Op.Eq}
| AND                                                                                           {Op.And}
| OR                                                                                            {Op.Or}
unop:
| NOT                                                                                           {Op.Neg}
(*cl_val:
| TRUE                                                                                          {SL.CL.CVAL.VBool true}
| FALSE                                                                                         {SL.CL.CVAL.VBool false}
| NUM                                                                                           {SL.CL.CVAL.VNum $1}*)



