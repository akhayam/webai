module D = Device.Device_Language
module SV = Variable.Server_Variable
module C = Language.Client_Language
module S = Language.Server_Language
module SSMap = Structures.SSMap
module SMap = Structures.SMap
module IIMap = Structures.IIMap

module type WORLD_ORACLE = sig
  module DLAN : Device.DEVICE_LANGUAGE
  module SI : Interpreter.SERVER_INTERPRETER
  type time = int
  type reaction = (SI.ST.value -> SI.ST.value)
  type world_oracle = (((DLAN.id * reaction) list) SMap.t) SSMap.t
  val world_reaction : world_oracle -> DLAN.actuator -> DLAN.id -> (DLAN.id * reaction) list
  module RDSet : Set.S with
      type elt = (SI.ST.SLAN.id * DLAN.id * SI.ST.SLAN.SVAR.vname * SI.ST.value)
  type pe =
  Actuator of (SI.ST.SLAN.id * (DLAN.actuator * DLAN.id) * time) (* physical event *)
  | PhysicalEvent of ((DLAN.id * SI.ST.value) * time)
  | Reading of RDSet.t
  | Split of (pe list * pe list)
  val has_evt : pe list -> D.id -> D.actuator -> bool
  val print_pe_list : pe list -> unit
end


module Mk_World_Oracle (DLAN : Device.DEVICE_LANGUAGE)(SI : Interpreter.SERVER_INTERPRETER) : WORLD_ORACLE =
struct
  module DLAN = DLAN
  module SI = SI

  type time = int
  type reaction = (SI.ST.value -> SI.ST.value)
  type world_oracle = (((DLAN.id * reaction) list) SMap.t) SSMap.t
  let world_reaction (wo : world_oracle) (act : DLAN.actuator) (id : DLAN.id) =
    SMap.find act (SSMap.find id wo)
  module VORD = struct
    type t = (SI.ST.SLAN.id * DLAN.id * SI.ST.SLAN.SVAR.vname * SI.ST.value)
    let (let*) (o : int) (f : unit -> 'a) : 'a =
      match o = 0 with
      | true -> f ()
      | false -> o
    let compare (((i1,j1),(d1,l1),x1,v1) : t) (((i2,j2),(d2,l2),x2,v2) : t) : int =
      let* _ = Int.compare i1 i2 in
      let* _ = Int.compare j1 j2 in
      let* _ = String.compare d1 d2 in
      let* _ = String.compare l1 l2 in
      let* _ = String.compare x1 x2 in
      SI.ST.value_compare v1 v2
    end
  module RDSet = Set.Make(VORD)
  type pe =
  Actuator of (SI.ST.SLAN.id * (DLAN.actuator * DLAN.id) * time) (* physical event *)
  | PhysicalEvent of ((DLAN.id * SI.ST.value) * time)
  | Reading of RDSet.t
  | Split of (pe list * pe list)

  let rec print_pe_list (l : pe list) : unit =
    match l with
    | [] -> ()
    | x :: t -> 
      (match x with
      | Actuator ((j,i),(a,(devn,loc)), t) ->
        Printf.printf "DeviceActuator => service = (%i, %i); device = (%s, %s); actuator = %s; time = %s \n" j i devn loc a (string_of_int t)
      | PhysicalEvent (((devn,loc),v),t) ->
        Printf.printf "DeviceSensor => device = (%s, %s); value = %s; time = %s \n" devn loc (SI.ST.to_string v) (string_of_int t)
      | Reading m -> 
        let ln = RDSet.cardinal m in
        let f = function (m : RDSet.t) ->
          RDSet.map (fun ((j,i),(devn,loc),x,v) ->
            Printf.printf "DeviceReading => service = (%i, %i);device = (%s, %s);variable = %s;value = %s \n" j i devn loc x (SI.ST.to_string v);
            ((j,i),(devn,loc),x,v)
            ) m in
        if ln > 1 then (Printf.printf "++++ Swappable Readings ++++\t";
                      Printf.printf "Length of Consecutive Swappable Readings = %i\n" ln;
                      let _ = f m in                 
                      Printf.printf "++++ End Swappable Readings ++++\n";
                      ) else let _ = f m in ()
      | Split (ll,lr) ->
        Printf.printf "++++ Split Left Thread Events ++++\t";
        print_pe_list ll;
        Printf.printf "++++ Split Right Thread Events ++++\t";
        print_pe_list lr);
      print_pe_list t(* Actuator_device_id -> (Actuator -> (Sensor_Device_id * value) list) *)
   let rec has_evt wl id act =
      match wl with
      | [] -> (false)
      | x :: t ->
        match x with
        | Actuator (_,(act',id'),_) -> if (D.compare id id' == 0 && String.compare act act' == 0) then
                                        (true)
                                       else
                                        (false || (has_evt t id act))
        | _ -> false || has_evt t id act
  
end

module World_Oracle =
  Mk_World_Oracle(Device.Device_Language)(Interpreter.Server_Interpreter)
module World_Oracle_F =
  Mk_World_Oracle(Device.Device_Language)(Interpreter.Server_Interpreter_F)
module World_Oracle_Intv =
  Mk_World_Oracle(Device.Device_Language)(Interpreter.Server_Interpreter_Intv)
module AWorld_Oracle =
  Mk_World_Oracle(Device.Device_Language)(Interpreter.AServer_interpreter)
module AWorld_Oracle_Oct =
  Mk_World_Oracle(Device.Device_Language)(Interpreter.AServer_interpreter_Oct)
