module type OP =
  sig
    type bop =
      Add | Sub | Mul (* arith ops *)
      | LEq | LT | GEq | GT | Eq | NEq (* comparison*)
      | And | Or (* logical ops *)
    and uop = Neg
    val pp_bop : out_channel -> bop -> unit
      
    val pp_uop : out_channel -> uop -> unit
  end

module Op : OP =
  struct
    type bop =
      Add | Sub | Mul (* arith ops *)
    | LEq | LT | GEq | GT | Eq | NEq (* comparison*)
    | And | Or (* logical ops *)
    and uop = Neg
    let pp_bop c o= Printf.fprintf c "%s" 
        (match o with
        Add -> "+" | Sub -> "-" | Mul -> "*"
        | LEq -> "<=" | LT -> "<" | GEq -> ">="
        | GT -> ">" | Eq -> "=" | NEq -> "!="
        | And -> "/\\" | Or -> "\\/" )
        
    let pp_uop c o= Printf.fprintf c "%s" 
      (match o with
      Neg -> "!")
  end