module W = Web_utils.Web
module O = Op.Op

open W
open O
open Structures

module type CLIENT_VALUE =
  sig
  type t = VNum of int | VBool of bool
  val to_string : t -> string
  val pp_value : out_channel -> t -> unit
  val compare : t -> t -> int(*
  val mk_const_bool : unit -> t
  val mk_const_int : unit -> t*)
  end

module Client_Value : CLIENT_VALUE =
  struct
    type t = VNum of int | VBool of bool
    let to_string (v : t) : string =
      match v with
      | VNum i -> "NUM "^(string_of_int i)
      | VBool b -> "BOOL "^(string_of_bool b)
    let pp_value c v = Printf.fprintf c "%s"
       (match v with | VNum i -> string_of_int i | VBool b -> string_of_bool b)
    let compare v1 v2 =
      match (v1,v2) with
      | (VNum i1, VNum i2) -> Int.compare i1 i2
      | (VBool i1, VBool i2) -> Bool.compare i1 i2
      | (VNum _, VBool _) -> 1
      | _ -> -1 
    
    (*let mk_const_int () = Random.init 10; VNum (Random.int 10)
    let mk_const_bool () = Random.init 10; VBool (Random.bool ())*)
  end

  module type CLIENT_LANGUAGE =
  sig
    module CVAR : Variable.CLIENT_VARIABLE 
    module CVAL : CLIENT_VALUE
    type expr =
      Value of CVAL.t
      | Var of CVAR.vname
      | BOp of (bop * expr * expr)
      | UOp of (uop * expr)
    and stmt =
      Ass of (CVAR.vname * expr)
      | If of (expr * block * block)
      | While of (expr * block)
      | Skip
      | Return of expr
      | Call of (url * CVAL.t * (CVAR.vname * block)) (* calling an url with a value. Last parameter is a callback handler *)
    and locstmt = int * stmt
    and block = stmt list
    and id = int
    and status = | Done | Calling of (W.url * (CVAR.vname * block)) | Running
    and ret = V of CVAL.t | S of block
    and client_exec_type = Boot | NonBoot of ret
    val vars_in_expr : expr -> SSet.t
    val stmts_in_block : block -> stmt list  
    val mk_var_set : block -> SSet.t -> (stmt -> SSet.t -> SSet.t) -> SSet.t
    val vars : block -> SSet.t
    
  end
  

module Client (CVAR : Variable.CLIENT_VARIABLE) (CVAL : CLIENT_VALUE) :
  CLIENT_LANGUAGE =
struct
  module CVAL = CVAL
  module CVAR = CVAR
  type expr =
    Value of CVAL.t
    | Var of CVAR.vname
    | BOp of (bop * expr * expr)
    | UOp of (uop * expr)
  and stmt =
    Ass of (CVAR.vname * expr)
    | If of (expr * block * block)
    | While of (expr * block)
    | Skip
    | Return of expr
    | Call of (url * CVAL.t * (CVAR.vname * block)) (* calling an url with a value. Last parameter is a callback handler *)
  and locstmt = int * stmt
  and block = stmt list
  and id = int
  let vars_in_expr (e : expr) : SSet.t =
    let rec aux (e : expr) (vars : SSet.t) : SSet.t =
      (match e with
      | Var x -> SSet.add x vars
      | BOp (_,e1,e2) -> aux e2 (aux e1 vars)
      | UOp (_, e') -> aux e' vars
      | _  -> vars) in
    aux e (SSet.empty)
  let stmts_in_block (b : block) : stmt list =
    let rec aux (b : block) (l : stmt list) : stmt list = 
      match b with
      | [] -> l
      | s::t -> (aux t (l@[s])) in
      aux b []

  let mk_var_set (b : block) (vars : SSet.t) (f : stmt -> SSet.t -> SSet.t) =
    List.fold_left (fun s1 s2 -> SSet.union s1 s2)
                    vars
                    (List.map (fun s -> f s SSet.empty) (stmts_in_block b))
  
  let vars (b : block) : SSet.t =
    let rec aux_stmt (s : stmt) (vars : SSet.t) : SSet.t =
      (match s with
      | Ass (x,e) -> SSet.union (SSet.add x vars) (vars_in_expr e)
      | Skip -> vars
      | Return e -> SSet.union vars (vars_in_expr e)
      | While (e, b) -> SSet.union (vars_in_expr e) (mk_var_set b vars aux_stmt)
      | If (e, b1, b2) ->
          SSet.union (vars_in_expr e)
                     (mk_var_set b2 (mk_var_set b1 vars aux_stmt) aux_stmt)
      | _ -> SSet.empty) in
      mk_var_set b SSet.empty aux_stmt
  type status = | Done | Calling of (W.url * (CVAR.vname * block)) | Running
  type ret = V of CVAL.t | S of block
  type client_exec_type = Boot | NonBoot of ret
end

module Client_Language = Client(Variable.Client_Variable)(Client_Value)

module type SERVER_VALUE =
  sig
  type t = VNum of int | VBool of bool | VClos of Client_Language.block
  val pp_value : out_channel -> t -> unit
  (*val mk_const_bool : unit -> t
  val mk_const_int : unit -> t*)
  end

module Server_Value : SERVER_VALUE =
  struct
    type t = VNum of int | VBool of bool | VClos of Client_Language.block
  let pp_value c v = Printf.fprintf c "%s"
    (match v with
    | VNum i -> Int.to_string i
    | VBool b -> Bool.to_string b
    | VClos _ -> "Closure. Result of a tilde expression")
  
  (*let mk_const_int () = Random.init 10; VNum (Random.int 10)
  let mk_const_bool () = Random.init 10; VBool (Random.bool ())*)
  end


  module type SERVER_LANGUAGE = sig
    module SVAL : SERVER_VALUE
    module SVAR : Variable.SERVER_VARIABLE
    module CL : CLIENT_LANGUAGE
    module DL : Device.DEVICE_LANGUAGE
    type tilde_lan = CL.block
      (*| Split of (status * status)*)
    and ret = | V of SVAL.t | S of CL.block
    and expr =
    Value of SVAL.t
    | Var of SVAR.vname
    | BOp of (bop * expr * expr)
    | UOp of (uop * expr)
    | Tilde of tilde_lan
    and stmt =
    Ass of (SVAR.vname * expr)
    | If of (expr * block * block)
    | While of (expr * block)
    | Skip
    | Return of expr
    | Act of (DL.actuator * DL.id * permission) (* issuing an actuation order to a device *)
    | Get of (SVAR.vname * DL.id * permission) (* reading request to a device*)
    | Wait of (SVAR.vname)
    and locstmt = int * stmt
    and block = stmt list
    and id = int * int      
    val expr_neg : expr -> expr
    val vars_in_expr : expr -> SSet.t
    val stmts_in_block : block -> stmt list
    val mk_var_set : block -> SSet.t -> ( stmt -> SSet.t -> SSet.t) -> SSet.t
    val vars : block -> SSet.t
    (*val get_first_line : block -> int*)
  end
  


module Server (SVAL : SERVER_VALUE) (SVAR : Variable.SERVER_VARIABLE)
              (CL : CLIENT_LANGUAGE) (DL : Device.DEVICE_LANGUAGE) = struct
  module SVAL = SVAL
  module SVAR = SVAR
  module CL = CL
  module DL = DL
  type tilde_lan = CL.block
  type tilde_stmt = CL.stmt List.t
    (*| Split of (status * status)*)
  and ret = | V of SVAL.t | S of CL.block
  and expr =
  Value of SVAL.t
  | Var of SVAR.vname
  | BOp of (bop * expr * expr)
  | UOp of (uop * expr)
  | Tilde of tilde_lan
  and stmt =
  Ass of (SVAR.vname * expr)
  | If of (expr * block * block)
  | While of (expr * block)
  | Skip
  | Return of expr
  | Act of (DL.actuator * DL.id * permission) (* issuing an actuation order to a device *)
  | Get of (SVAR.vname * DL.id * permission) (* reading request to a device*)
  | Wait of (SVAR.vname)
  and locstmt = int * stmt
  and block = stmt list
  and id = int * int

  let rec expr_neg (e : expr) : expr =
    match e with
    | Value (VBool b) -> (match b with | true -> Value (VBool false)
                                       | false -> Value (VBool true))
    | Value (VNum n) -> Value (VNum (n * -1))
    | UOp (Neg, e) -> e
    | BOp (bop, e1, e2) ->
      (match bop with
      | Add | Sub | Mul -> failwith "ERROR : cannot negate arithmetic expressions"
      | Eq -> BOp (NEq,e1,e2)
      | NEq -> BOp (Eq,e1,e2)
      | LT -> BOp (GEq,e1,e2)
      | GT -> BOp (LEq,e1,e2)
      | LEq -> BOp (GT,e1,e2)
      | GEq -> BOp (LT,e1,e2)
      | And -> BOp (Or,expr_neg e1,expr_neg e2)
      | Or -> BOp (And,expr_neg e1,expr_neg e2))
    | _ -> failwith "ERROR : cannot negate a closure"
   
  let vars_in_expr (e : expr) : SSet.t =
    let rec aux (e : expr) (vars : SSet.t) : SSet.t =
      (match e with
      | Var x -> SSet.add x vars
      | BOp (_,e1,e2) -> aux e2 (aux e1 vars)
      | UOp (_, e') -> aux e' vars
      | _  -> vars) in
    aux e (SSet.empty)
  let stmts_in_block (b : block) : stmt list =
    let rec aux (b : block) (l : stmt list) : stmt list = 
      match b with
      | [] -> l
      | s::t -> (aux t (l@[s])) in
      aux b []
  let mk_var_set (b : block) (vars : SSet.t) (f : stmt -> SSet.t -> SSet.t) =
    List.fold_left (fun s1 s2 -> SSet.union s1 s2)
                    vars
                    (List.map (fun s -> f s SSet.empty) (stmts_in_block b))
    
  let vars (b : block) : SSet.t =
    let rec aux_stmt (s : stmt) (vars : SSet.t) : SSet.t =
      (match s with
      | Ass (x,e) -> SSet.union (SSet.add  x vars) (vars_in_expr e)
      | Skip -> vars
      | Return e -> SSet.union vars (vars_in_expr e)
      | While (e, b) -> SSet.union (vars_in_expr e) (mk_var_set b vars aux_stmt)
      | If (e, b1, b2) ->
          SSet.union (vars_in_expr e)
                     (mk_var_set b2 (mk_var_set b1 vars aux_stmt) aux_stmt)
      | Get (x,_,_) -> SSet.add x vars
      | _ -> SSet.empty) in
      mk_var_set b SSet.empty aux_stmt
  let (let*) o f =
      match o with
      | Some x -> f x
      | None -> None
  (*let get_first_line (b : block) : int =
    match b with
    | [] -> 10
    |  (i,_)::_ -> i*)
end

module Server_Language = Server(Server_Value)(Variable.Server_Variable)
                               (Client_Language)(Device.Device_Language)
