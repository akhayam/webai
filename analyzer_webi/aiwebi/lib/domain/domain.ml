open Apron

module type PRE_APRON =
  sig
    type t
    type atyp = | BOX | OCT | POLKA | POLKA_EQ
    val typ : atyp
    val man: t Manager.t
  end
module PA_box =
  (struct
    type t = Box.t
    type atyp = | BOX | OCT | POLKA | POLKA_EQ
    let typ = BOX
    let man: t Manager.t =
      Box.manager_alloc ()
  end: PRE_APRON)
module PA_oct =
  (struct
    type t = Oct.t
    type atyp = | BOX | OCT | POLKA | POLKA_EQ
    let typ = OCT
    let man: t Manager.t =
      Oct.manager_alloc ()
  end: PRE_APRON)
module PA_polka =
  (struct
    type t = Polka.strict Polka.t
    type atyp = | BOX | OCT | POLKA | POLKA_EQ
    let typ = POLKA
    let man: t Manager.t =
      Polka.manager_alloc_strict ()
  end: PRE_APRON)
module PA_polka_eq =
  (struct
    type t = Polka.equalities Polka.t
    type atyp = | BOX | OCT | POLKA | POLKA_EQ
    let typ = POLKA_EQ
    let man: t Manager.t =
      Polka.manager_alloc_equalities ()
  end: PRE_APRON)

module E = Apron.Environment
type 'a man = 'a Apron.Manager.t
type env = E.t
module A = Abstract1