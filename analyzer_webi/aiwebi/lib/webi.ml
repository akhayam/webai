
module SSMap = Structures.SSMap
module SMap = Structures.SMap
module SSet = Structures.SSet
module IMap = Structures.IMap
module IIMap = Structures.IIMap

let (let*) ((w,s) : ('a Option.t*string))(f : 'a -> 'b): 'b =
  match w with
  | Some v -> f v
  | None -> failwith s
let (let&) ((w,s) : ('a Option.t*string)) (f : 'a -> 'a): 'a =
  match w with
  | Some v -> f v
  | None -> failwith s
let opt_apply (fo: (('a -> 'b) Option.t * string)) (p : 'a): 'b = (let* f = fo in f p)
let opt_extr (w: ('a Option.t * string)): 'a = (let& v = w in v)

module type WEBI_INTERPRETER = sig
  (*module SI : Concrete_interpreter.SERVER_INTERPRETER*)
  module CI : Interpreter.CLIENT_INTERPRETER
  module DLAN : Device.DEVICE_LANGUAGE
  module WEB : Web_utils.WEB
  module WO : World.WORLD_ORACLE
  module SI := WO.SI
  (* Ast *)
  type tier =
  | Service of (WEB.url * WEB.hostname * SI.ST.SLAN.SVAR.vname *
    SI.ST.SLAN.stmt List.t) (* service http://.. (x) := prog*)
  | Device of (DLAN.id * WO.SI.ST.value *
    (DLAN.actuator * WO.SI.ST.value) List.t) (*device (window,kitchen) init 25 := [open -> 1; close -> 0]*)
  | Client of (WEB.url * SI.ST.CLAN.CVAR.typ * int) (*client 1 calls http://.. with param INT*)
  | Oracle of ((DLAN.id * DLAN.actuator) * (DLAN.id * CI.CL.CVAL.t)) (* oracle says : (oven,kitchen) is on --> (thermometer,kitchen) is 25*)

  type program = tier List.t

  type prg = {
    services : (WEB.url * WEB.hostname * SI.ST.SLAN.SVAR.vname *
      SI.ST.SLAN.stmt List.t) List.t;
    devices : (DLAN.id * WO.SI.ST.value *
      (DLAN.actuator * WO.SI.ST.value) List.t) List.t;
    clients : (WEB.url * SI.ST.CLAN.CVAR.typ * int) List.t;
    oracle : ((DLAN.id * DLAN.actuator) * (DLAN.id * CI.CL.CVAL.t)) List.t;
  }
  type webservices = (SI.ST.SLAN.SVAR.vname * SI.ST.SLAN.block) SMap.t
  module InitSet : Set.S with type elt = (WEB.url * SI.ST.CLAN.CVAR.typ * int)
  type inits = InitSet.t (*url * type_v * id*)
  type device = {
    semantics : WO.SI.ST.value SMap.t;
    state : WO.SI.ST.value 
    }
  type devices = device SSMap.t
  type services ={
    srv_done : SI.configuration IIMap.t;
    srv_run : SI.configuration IIMap.t;
    srv_act : SI.configuration IIMap.t;
    srv_get : SI.configuration IIMap.t;
  }
  type callback = {var : CI.CL.CVAR.vname; prg : CI.CL.block}
  type thunk = {state : CI.ST.t; prg : CI.CL.block}
  type cc = {conf : CI.configuration;
             callbacks : callback IMap.t;
             thunks : thunk IMap.t}
  type clients ={
    cl_done : cc IMap.t;
    cl_call : cc IMap.t;
    cl_run : cc IMap.t;
    cl_boot : cc IMap.t
  }
  type trace_element =
  | SI of SI.ST.SLAN.id
  | SS of SI.ST.SLAN.id
  | RS of SI.ST.SLAN.id
  | DR of (SI.ST.SLAN.id * DLAN.id)
  | DA of (SI.ST.SLAN.id * DLAN.id * DLAN.actuator)
  | Iteration of int
  (* Wi configuration *)
  type conf = {
    wl : WO.pe list;
    inits : inits;
    services : services;
    clients : clients;
    devices : devices;
    webservices : webservices;
    wo : WO.world_oracle;
    debug : trace_element list}
  val get_world_log : conf -> WO.pe list
  val get_debug : conf -> trace_element list
  type step = | Init | SrvStep | Ret | Devs | CS | CC | Run | Exit
  type confs = (conf * step) List.t
  val print_trace_element_list : trace_element list -> unit
  val execute_webi : conf list -> step -> int -> (conf * step * int) list
  val observe_actuation : (conf*step*int) list -> WO.DLAN.id ->
    WO.DLAN.actuator -> unit
end

module Mk_Webi_Interpreter (CI : Interpreter.CLIENT_INTERPRETER)
                           (DLAN : Device.DEVICE_LANGUAGE)
                           (WEB : Web_utils.WEB)
                           (WO : World.WORLD_ORACLE) : WEBI_INTERPRETER =
struct

  module CI = CI
  module DLAN = DLAN
  module WEB = WEB
  module WO = WO
  module SI = WO.SI
  (* Ast *)
  type tier =
  | Service of (WEB.url * WEB.hostname * SI.ST.SLAN.SVAR.vname *
    SI.ST.SLAN.stmt List.t) (* service http://.. (x) := prog*)
  | Device of (DLAN.id * WO.SI.ST.value *
    (DLAN.actuator * WO.SI.ST.value) List.t) (*device (window,kitchen) init 25 := [open -> 1; close -> 0]*)
  | Client of (WEB.url * SI.ST.CLAN.CVAR.typ * int) (*client 1 calls http://.. with param INT*)
  | Oracle of ((DLAN.id * DLAN.actuator) * (DLAN.id * CI.CL.CVAL.t)) (* oracle says : (oven,kitchen) is on --> (thermometer,kitchen) is 25*)

  type program = tier List.t

  type prg = {
    services : (WEB.url * WEB.hostname * SI.ST.SLAN.SVAR.vname *
      SI.ST.SLAN.stmt List.t) List.t;
    devices : (DLAN.id * WO.SI.ST.value *
      (DLAN.actuator * WO.SI.ST.value) List.t) List.t;
    clients : (WEB.url * SI.ST.CLAN.CVAR.typ * int) List.t;
    oracle : ((DLAN.id * DLAN.actuator) * (DLAN.id * CI.CL.CVAL.t)) List.t;
  }
(*
  let mk_webi_prg (l : tier list) : prg =
    List.fold_left
       (fun (p1 : prg) (p2 : prg) : prg ->
          {services = p1.services@p2.services;
           devices = p1.devices@p2.devices;
           oracle = p1.oracle@p2.oracle;
           clients = p1.clients@p2.clients};)
          {services = []; devices = []; clients = []; oracle = []}
          (List.map (function | Service s -> {services = [s];devices = [];clients = []; oracle =[]}
                              | Device d -> {services = [];devices = [d];clients = []; oracle =[]}
                              | Client c -> {services = [];devices = [];clients = [c]; oracle =[]}
                              | Oracle o -> {services = [];devices = [];clients = []; oracle = [o]}) l)*)

  type webservices = (SI.ST.SLAN.SVAR.vname * SI.ST.SLAN.block) SMap.t
                              
  (*let mk_webservices (p : prg) : webservices =
    List.fold_left
      (fun (s1 : webservices)
           (s2 : webservices) ->
             (SMap.union (fun _ _ _ ->
                             failwith "ERROR: some services have the same URL.")
                         s1 s2))
      SMap.empty
      (List.map (function (u,x,b) -> SMap.add (WEB.string_of_url u) (x,b) SMap.empty)
                p.services)*)

  module VORD_InitSet =struct  
    type t = (WEB.url * SI.ST.CLAN.CVAR.typ * int)
    let compare e1 e2 =
      match (e1,e2) with
      | (_,_,i1), (_,_,i2) -> Int.compare i1 i2        
  end

  module InitSet = Set.Make(VORD_InitSet)

  (* Inits *)
  type inits = InitSet.t (*url * type_v * id*)

  (*let mk_inits (p : prg) : inits =
    List.fold_left
      (fun (im1 : inits) (im2 : inits) -> InitSet.union im1 im2)
      InitSet.empty
      (List.map (function (u,t,i) ->
                    InitSet.add (u,t,i) InitSet.empty) p.clients)*)



  (* IC - devices *)  
  type device = {
    semantics : WO.SI.ST.value SMap.t;
    state : WO.SI.ST.value
    }
  type devices = device SSMap.t
  (*let mk_devices (p : prg) : devices =
    List.fold_left
      (fun d1 d2 -> SSMap.union (fun _ _ _ -> failwith "ERROR: some devices have the same ID") d1 d2)
      SSMap.empty
      (List.map (function (id,v,l) ->
                    let sem = List.fold_left (fun s1 s2 -> SMap.union (fun _ _ _ -> failwith "ERROR: a device defines an actuation multiple times") s1 s2)
                              SMap.empty
                              (List.map (function (a,v) -> SMap.add a v SMap.empty) l) in
                    SSMap.add id {semantics = sem; state = v} SSMap.empty) p.devices)*)

  (* SS *)
  type services ={
    srv_done : SI.configuration IIMap.t;
    srv_run : SI.configuration IIMap.t;
    srv_act : SI.configuration IIMap.t;
    srv_get : SI.configuration IIMap.t;
  }

  type callback = {var : CI.CL.CVAR.vname; prg : CI.CL.block}
  type thunk = {state : CI.ST.t; prg : CI.CL.block}
  type cc = {conf : CI.configuration; callbacks : callback IMap.t;
    thunks : thunk IMap.t}
  (* CC *)
  (*original client id -> client configuration*)
  type clients ={
    cl_done : cc IMap.t;
    cl_call : cc IMap.t;
    cl_run : cc IMap.t;
    cl_boot : cc IMap.t
  }
  

  type trace_element =
  | SI of SI.ST.SLAN.id
  | SS of SI.ST.SLAN.id
  | RS of SI.ST.SLAN.id
  | DR of (SI.ST.SLAN.id * DLAN.id)
  | DA of (SI.ST.SLAN.id * DLAN.id * DLAN.actuator)
  | Iteration of int

  let rec print_trace_element_list (l : trace_element list) : unit =
    match l with
    | [] -> Printf.printf "---- End Execution Trace ----\n";
    | x :: t ->
      (match x with
      | SI (j,i) -> Printf.printf "=> ServiceInit service (%i,%i)\n" j i
      | SS (j,i) -> Printf.printf "=> ServiceStep service (%i,%i)\n" j i
      | RS (j,i) -> Printf.printf "=> RetService service (%i,%i)\n" j i
      | DR ((j,i),(nd,ld)) -> Printf.printf 
        "=> DeviceReading service (%i,%i), device (%s,%s)\n" j i nd ld
      | DA ((j,i),(nd,ld),a) ->Printf.printf 
          "=> DeviceActuator service (%i,%i), device (%s,%s), actuator %s\n"
            j i nd ld a
      | Iteration i -> Printf.printf "\nScheduler's iteration #%i\n\n" i);
      print_trace_element_list t

  (* Wi configuration *)
  type conf = {
    wl : WO.pe list;
    inits : inits;
    services : services;
    clients : clients;
    devices : devices;
    webservices : webservices;
    wo : WO.world_oracle;
    debug : trace_element list}

  let get_world_log (c: conf) : WO.pe list = c.wl
  let get_debug (c : conf) : trace_element list = c.debug
  (*
  let mk_webi_conf (p : tier list) : conf =
    let p = mk_webi_prg p in
    let dvs = mk_devices p in
    let ws = mk_webservices p in
    let inits = mk_inits p in
    {
      wl = [];
      inits = inits;
      services = {srv_done = IIMap.empty; srv_run = IIMap.empty; srv_get = IIMap.empty; srv_act =IIMap.empty};
      clients = {cl_done = IMap.empty; cl_run = IMap.empty; cl_call = IMap.empty; cl_boot = IMap.empty};
      devices = dvs;
      webservices = ws;
      wo = SSMap.empty;
      debug = []
    }*)

    let mk_service ((u,t) : (WEB.url * SI.ST.CLAN.CVAR.typ))
      (ws : webservices) : SI.configuration =
      match SMap.find_opt (WEB.string_of_url u) ws with
          | Some (x,p) ->
             {
              prg = p;
              pp = 0;
              status = SI.Running;
              state = SI.ST.make p x (SI.ST.mk_init_value t);
              expr = [] (* setting a random value bound to x *)
              }
          | None ->
              failwith ("ERROR: URL "^(WEB.string_of_url u)^"does not exist")

    let mk_boot_client (u : WEB.url) : CI.configuration =
      {
        origin_url = u;
        prg = Boot;
        pp = 0;
        status = CI.CL.Running;
        state = CI.ST.new_state;
      }
    
    let add_service (ss : SI.configuration) (id : SI.ST.SLAN.id)
      (srvs : services) : services =
      match ss.status with
      | Running -> {srvs with srv_run = IIMap.add id ss srvs.srv_run }
      | Done _ -> {srvs with srv_done = IIMap.add id ss srvs.srv_done}
      | Actuating _ -> {srvs with srv_act = IIMap.add id ss srvs.srv_act}
      | Reading _ -> {srvs with srv_get = IIMap.add id ss srvs.srv_get}
      | Ret _ -> {srvs with srv_run = IIMap.add id ss srvs.srv_run}


      
    let get_client (cc : clients) (id : CI.CL.id) : (clients * cc) =
      match (IMap.mem id cc.cl_run, IMap.mem id cc.cl_boot,
        IMap.mem id cc.cl_call, IMap.mem id cc.cl_done) with
      | (true,false,false,false) -> ({cc with cl_run = IMap.remove id cc.cl_run},
          opt_extr (IMap.find_opt id cc.cl_run, "get_client run"))
      | (false,true,false,false) -> ({cc with cl_boot = IMap.remove id cc.cl_boot},
          opt_extr (IMap.find_opt id cc.cl_boot, "get_client boot"))
      | (false,false,true,false) -> ({cc with cl_call = IMap.remove id cc.cl_call},
          opt_extr (IMap.find_opt id cc.cl_call,"get_client call"))
      | (false,false,false,true) -> ({cc with cl_done = IMap.remove id cc.cl_done},
          opt_extr (IMap.find_opt id cc.cl_done,"get_client done"))
      | _ -> failwith "Error: a client belong to more than 1 partition"

    let get_callback (id : CI.CL.id) (cc : cc) : (callback * cc) =
      (opt_extr (IMap.find_opt id cc.callbacks, "Error : get_callback"),
        {cc with callbacks = IMap.remove id cc.callbacks})
    let get_service (ss : services) (status : string) :
      ((SI.ST.SLAN.id * SI.configuration) * services) option =
      match status with
      | "done" -> 
          (match IIMap.choose_opt ss.srv_done with
          | Some (id,ss_i) ->
             let ss_done' = IIMap.remove id ss.srv_done in
             Some ((id,ss_i),{ss with srv_done = ss_done'})
          | None -> None)
      | "act" -> 
          (match IIMap.choose_opt ss.srv_act with
          | Some (id,ss_i) ->
            let ss_act' = IIMap.remove id ss.srv_act in
            Some ((id,ss_i),{ss with srv_act = ss_act'})
          | None -> None)
      | "get" -> 
          (match IIMap.choose_opt ss.srv_get with
          | Some (id,ss_i) ->
              let ss_get' = IIMap.remove id ss.srv_get in
              Some ((id,ss_i),{ss with srv_get = ss_get'})
          | None -> None)                 
      | _ -> failwith "ERROR : todo"

    
  let parse_value (v : SI.ST.SLAN.SVAL.t) : CI.ST.value =
    match v with
    | VNum i -> CI.ST.to_avalue (CI.ST.CVAL.VNum  i)
    | VBool b -> CI.ST.to_avalue (CI.ST.CVAL.VBool b)
    | VClos _ -> failwith "ERROR : Service must return a value"
  let parse_client (r : SI.ST.SLAN.ret) : CI.CL.ret =
    match r with
    | V (VNum i) -> V (VNum i)
    | V (VBool b) -> V (VBool b)
    | S _ -> failwith "ERROR : Tilde not supported"
    | V _ -> failwith "ERROR : impossible to have V (VClos) in the type ret"
    
  (*let parse_dev (v : WO.CI.ST.value) : WO.SI.SL.SVAL.t =
    match WO.CI.ST.of_value v with
    | VBool b -> VBool b
    | VNum i -> VNum i*)

    let add_thunk (cc : cc)(id : CI.CL.id) (var : CI.CL.CVAR.vname)
      (status : SI.status) (prg : CI.CL.block): cc =
      {cc with thunks = IMap.add id
                                 {state = CI.ST.update (CI.ST.new_state)
                                   (CI.ST.key_of_string var)
                                   (match status with
                                    | SI.Done (SI.ST.SLAN.V v) -> parse_value v
                                    | _ -> failwith "ERROR : status supposed to be done")
                                    ;
                                  prg = prg}
                                  cc.thunks}

    let add_client (cs : clients)(cc : cc) (id : CI.CL.id) : clients =
      match cc.conf.status with
      | Done -> {cs with cl_done = IMap.add id cc cs.cl_done}
      | Running -> {cs with cl_run = IMap.add id cc cs.cl_run}
      | Calling _ ->  {cs with cl_call = IMap.add id cc cs.cl_call}


    let upd_service_status (s : services) (ss : SI.configuration)
      (id : SI.ST.SLAN.id) (status : SI.status) : services =
      match status with
      | Running |Ret _ ->
        {s with srv_run = IIMap.add id {ss with status = status} s.srv_run}
      | Reading _ ->
        {s with srv_get = IIMap.add id {ss with status = status} s.srv_get}
      | Actuating _ ->
        {s with srv_act = IIMap.add id {ss with status = status} s.srv_act}
      | Done _ -> 
        {s with srv_done = IIMap.add id {ss with status = status} s.srv_done}

    let upd_actuator_mem (ds : devices) (d :device) (id : DLAN.id)
      (v : WO.SI.ST.value) : devices =
      SSMap.add id {d with state = v } ds

    let upd_sensor_mem (wo : WO.world_oracle) (devices : devices)
      (id : DLAN.id) (act : DLAN.actuator) : devices =
      List.fold_left (fun (m : device SSMap.t) ((id,d) : (DLAN.id * device)) ->
       SSMap.add id d m)
       devices
      (List.map (fun ((id,f) : (WO.DLAN.id * WO.reaction)) : (DLAN.id * device) ->
                     let d = opt_extr (SSMap.find_opt id devices,"upd_sensor_mem d") in
                     (id,{d with state = f d.state}))
                     (WO.world_reaction wo act id))
    let add_act_worldlog (wl : WO.pe list) (k : SI.ST.SLAN.id)
      (act : DLAN.actuator) (id : DLAN.id) : WO.pe list =
      wl@[WO.Actuator (k, (act, id), (List.length wl) + 1)]

    let add_sensorstate_wl (wo : WO.world_oracle) (act : DLAN.actuator)
      (id : DLAN.id) (current_time : WO.time) (c : conf): WO.pe list =
      List.map (fun ((ids,_) : (WO.DLAN.id * WO.reaction)) : WO.pe ->
                      PhysicalEvent ((ids,(let d = SSMap.find ids c.devices in
                        d.state)),current_time + 1))
        (SMap.find act (SSMap.find id wo))

    let (let*) o f =
      match o with
      | Some x -> f x
      | None -> None
    (*
    let (let&) (o : 'a -> ('b *'a)) (f : 'b -> ('a -> ('c*'a))) (s : 'a) : ('c * 'a) =
        let (v,s') = o s in
        f v s'
 
    let string_of_status (s : SI.SL.status) : string =
      match s with
      | SI.SL.Reading _ -> "reading"
      | SI.SL.Actuating _ -> "actuating"
      | SI.SL.Done _ -> "done"
      | SI.SL.Ret _ -> "ret"
      | SI.SL.Running -> "running"*)

    let rule_ServiceInit (c : conf) : conf option =
      let* (u,t,i) = InitSet.choose_opt c.inits in
      let ss_i = mk_service (u,t) c.webservices in
      Some {c with 
            services = {c.services with
                        srv_run = IIMap.add (i,0) ss_i c.services.srv_run};
            inits = (InitSet.remove (u,t,i) c.inits);
            clients = {c.clients with cl_boot = IMap.add i
                         {conf = (mk_boot_client u);
                          callbacks = IMap.empty;
                          thunks = IMap.empty} c.clients.cl_boot};
            debug = c.debug@[(SI (i,0))]}

    let rule_ServiceStep (c : conf) : (conf list) option =
      let* (id,ss_i) = IIMap.choose_opt c.services.srv_run in
      let l_ss_i' = SI.eval ss_i in
      let c' = {c with services = {c.services with srv_run = IIMap.remove id
                                   c.services.srv_run};
                       debug = c.debug@[SS id]} in
      Some (List.map
              (fun ss_i' -> {c' with services =
                                       (add_service ss_i' id c'.services)})
              l_ss_i')
      
    let rule_RetService (c:conf) : conf option=
      let* (((j,i),ss_i),services') = get_service c.services "done" in (* client id * callback id *)
      let (clients', cc_j) = get_client c.clients j in
      match cc_j.conf.prg with
      | Boot -> Some
        ({c with services = services';
                 clients = add_client clients' 
                                     {cc_j with conf =
                                       {cc_j.conf with prg = NonBoot
                                          (match ss_i.status with 
                                           | Done d -> (parse_client d)
                                           | _ -> failwith "ERROR : Service must be done") }}
                                      j;
                 debug = c.debug@[RS (j,i)]})
      | NonBoot _ -> Some
        (let (b_i,cc_j') = get_callback i cc_j in
        {c with services = services';
                clients = add_client clients' 
                                     (add_thunk cc_j' i b_i.var ss_i.status b_i.prg)
                                     j;
                debug = c.debug@[RS (j,i)]})

    let pick_act (c : conf) (k : SI.ST.SLAN.id) =
      (IIMap.find k c.services.srv_act,
        {c.services with
         srv_act = IIMap.remove k c.services.srv_act})
    let (let@) = fun w f ->
      match w with
      | SI.Actuating (act,id_d) -> f (act,id_d)
      | _ -> failwith "ERROR : service must actuate in device actuator rule"

    let rec latest_act (l : WO.pe List.t) : WO.pe Option.t =
      match List.rev l with
      | [] -> None
      | x :: t ->
        match x with
        | Actuator _ -> Some x
        | _ -> latest_act t
    let rule_DeviceSensor (c : conf) : conf =
      match latest_act c.wl with
      | None -> c
      | Some a ->
        match a with
        | Actuator (_,(act,d_id),_) ->
          (*Printf.printf "act %s\n" act;*)
          let c' = {c with devices = upd_sensor_mem c.wo c.devices d_id act} in
          {c' with
            wl = c'.wl@(add_sensorstate_wl c'.wo act d_id (List.length c'.wl + 1) c')}
        | _ -> failwith "ERROR: rule_DeviceSensor finds an actuator if there is"

      
    (*let rule_DeviceActuator (c : conf) (k : SI.ST.SLAN.id): conf =
      let (ss_i,services') = pick_act c k  in
      let@ (act,id_d) = ss_i.status in
      let d = SSMap.find id_d c.devices in
      let v = SMap.find act d.semantics in
      let c' = {c with
       devices = upd_sensor_mem c.wo (upd_actuator_mem c.devices d id_d v)
        id_d act;
       services = upd_service_status services' ss_i k Running;
       debug = c.debug@[DA (k,id_d,act)]} in
       {c' with 
       wl = (add_act_worldlog c'.wl k act id_d)@(add_sensorstate_wl c'.wo
        act id_d (List.length c'.wl + 1) c')}*)


    let rule_DeviceActuator (c : conf) (k : SI.ST.SLAN.id): conf =
      let (ss_i,services') = pick_act c k  in
      let@ (act,id_d) = ss_i.status in
      let d = SSMap.find id_d c.devices in
      let v = SMap.find act d.semantics in
      let c' = {c with
       devices = (upd_actuator_mem c.devices d id_d v);
       services = upd_service_status services' ss_i k Running;
       debug = c.debug@[DA (k,id_d,act)]} in
       {c' with 
       wl = (add_act_worldlog c'.wl k act id_d)}

    let pick_read (c : conf) (k : SI.ST.SLAN.id) =
      (IIMap.find k c.services.srv_get,
          {c.services with
           srv_get = IIMap.remove k c.services.srv_get})
    let (let$) = fun w f ->
      match w with
      | SI.Reading (x,d_id) -> f (x,d_id)
      | _ -> failwith "ERROR : service must read in device reading rule"

    let rule_DeviceReading (c : conf) (k : SI.ST.SLAN.id): conf =
      let (ss_i,services') = pick_read c k in
      let$ (x,d_id) = ss_i.status in
      let d = SSMap.find d_id c.devices in
      let v_s = d.state in
      {c with services = upd_service_status services'
              {ss_i with
               state = SI.ST.update ss_i.state
                          (SI.ST.key_of_string x) v_s}
                              k (Running);
               wl = (match List.rev c.wl with
                     | [] ->[Reading (WO.RDSet.add (k,d_id,x,v_s)
                             WO.RDSet.empty)]
                     | (Reading s)::t -> List.rev
                            ([WO.Reading (WO.RDSet.add (k,d_id,x,v_s) s)]@t)
                     | _::_ ->
                          c.wl@[Reading (WO.RDSet.add (k,d_id,x,v_s)
                                                      WO.RDSet.empty)]);
                 debug = c.debug@[DR (k,d_id)]}
      (*(match List.rev c.wl with
                      | [] -> [Reading [(k,d_id,x,v_s)]]
                      | (Reading l)::t -> (List.rev t)@[Reading (l@[(k,d_id,x,v_s)])]
                      | _::_ -> c.wl@[Reading [(k,d_id,x,v_s)]])*)

      (*let rule_DeviceSkip (c : conf) : conf = c*) (*an id fun that maps a pair (conf * step) to the next step unchanging the conf*)

      type step = | Init | SrvStep | Ret | Devs | CS | CC | Run | Exit

      type confs = (conf * step) List.t

      let rec rule_apply (c : conf) (step : step) (i : int) :
        (conf * step * int) list =
        match step with
        | Init ->
          let co = rule_ServiceInit c in
          List.flatten
            (List.map (fun (c' : conf option) ->
              match c' with
              | None -> [(c,SrvStep,i)]
              | Some c' -> (rule_apply c' Init i))
              [co])
           (*SI(SI(SI(SI(conf)))) Fixed Point on the concrete conf *)
        | SrvStep ->
          let cs = rule_ServiceStep c in
          (match cs with
          | Some cs ->
            List.flatten
              (List.map (fun c' ->
                match IIMap.is_empty c'.services.srv_run with
                | true -> [(c',Ret,i)]
                | false -> rule_apply c' SrvStep i) cs)
          | None -> [(c,Ret,i)])
        | Ret ->
          let cs = rule_RetService c in
          List.flatten
            (List.map (fun c' ->
              match c' with
              | None -> [(c,Devs,i)]
              | Some c' -> (rule_apply c' Ret i))
              [cs])
        | Devs -> (*all the possible acts 1 step @ all the possible reads 1 step. *)
          (
          let acts = List.map (fun c -> rule_DeviceSensor c) (apply_rules_with_service_id rule_DeviceActuator c
            (get_iimap_keys c.services.srv_act)) in (*handling empty list case*)
          let gets = (apply_rules_with_service_id rule_DeviceReading c
            (get_iimap_keys c.services.srv_get)) in (*handling empty list case*)
          let acts_next_step = map_step acts CS i in (*handling empty list case*)
          let gets_next_step = map_step gets CS i in (*handling empty list case*)
          let (_,acts_or_gets_alive) = partition_done_dev_confs acts gets in (*handling empty list case*)
          acts_next_step@ (*1 act step, for each act*)
            gets_next_step@ (*1 get step, for each step*)
              (*map_step acts_and_gets_done CS)@*) (*I am duplicating stuff here, not necessary*)
                (map_apply_step acts_or_gets_alive Devs i)) (*iterating on configuration which can still act or get*)


        | _ -> [({c with debug = c.debug@[Iteration (i+1)]},SrvStep,(i+1))]

      and get_iimap_keys (m : 'a IIMap.t) : IIMap.key List.t =
        List.map (fun (x,_) -> x)(IIMap.bindings m)
      and apply_rules_with_service_id (f : conf -> SI.ST.SLAN.id -> conf)
        (c : conf) (ks : SI.ST.SLAN.id list) : conf list =
        List.map (fun k -> f c k) ks
      and map_step (l : conf list) (s : step) (i : int) :
        (conf * step * int) list =
        List.map (fun x -> (x,s,i)) l
      and partition_done_dev_confs (acts : conf list) (gets : conf list) :
        (conf list*conf list) =
        List.partition
            (fun (x : conf) : bool -> (IIMap.is_empty x.services.srv_act) &&
                                      (IIMap.is_empty x.services.srv_get))
            (acts@gets)
      and map_apply_step (l : conf list) (s : step) (i : int) :
        (conf*step*int) list=
        List.flatten (List.map (fun c -> rule_apply c s i) l)

      let rec execute_webi (cs : conf list) (s :step) (i : int) :
        (conf * step * int) list = (*should use an accumulator for Exit configurations*)
        List.flatten
         (List.map (fun (c,s,i') ->
                        match s with 
                        | Exit -> [(c,Exit,i')] (*individual base case*)
                        | _ -> (let lexit = List.map (fun p -> IIMap.is_empty p)
                                                      [c.services.srv_get;
                                                       c.services.srv_act;
                                                       c.services.srv_run;
                                                       c.services.srv_done] in
                                match (List.fold_left (fun b1 b2 -> b1 && b2)
                                                      true lexit) with
                                | true -> [(c,Exit,i')]
                                | false -> execute_webi [c] s i'))(*individual recursion of active webi confs*)
                     (List.flatten (List.map (fun (c,s,i') -> rule_apply c s i')
                                   (List.map (fun c -> (c,s,i)) cs)))(*result of individual conf execution. out List*)
          )

    
  let observe_actuation (cl : (conf*step*int) list) (id : WO.DLAN.id)
    (act : WO.DLAN.actuator) : unit =
    let b = List.fold_left (fun b (c,_,_) ->
      let wl = c.wl in
      b || WO.has_evt wl id act) false cl in
    (match b with
     | false -> Printf.printf "#################### PROGRAM SECURE\n"
     | true -> Printf.printf "#################### PROGRAM INSECURE\n")


end


module Webi_Interpreter_Int =
  Mk_Webi_Interpreter(Interpreter.Client_Interpreter)
                     (Device.Device_Language)
                     (Web_utils.Web)
                     (World.World_Oracle)

module Webi_Interpreter_Float =
  Mk_Webi_Interpreter(Interpreter.Client_Interpreter_F)
                     (Device.Device_Language)
                     (Web_utils.Web)
                     (World.World_Oracle_F)

module Webi_Interpreter_Intv =
  Mk_Webi_Interpreter(Interpreter.Client_Interpreter_Intv)
                     (Device.Device_Language)
                     (Web_utils.Web)
                     (World.World_Oracle_Intv)
module Webi_AInterpreter =
  Mk_Webi_Interpreter(Interpreter.Client_Interpreter_Intv)
                     (Device.Device_Language)
                     (Web_utils.Web)
                     (World.AWorld_Oracle)
module Webi_AInterpreter_Oct =
  Mk_Webi_Interpreter(Interpreter.Client_Interpreter_Intv)
                     (Device.Device_Language)
                     (Web_utils.Web)
                     (World.AWorld_Oracle_Oct)


