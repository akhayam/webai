module type DEVICE_LANGUAGE =
  sig
    type id = (string * string) (* (name * location)*)
    type actuator = string
    type typ = | Actuator | Sensor
    val compare : id -> id -> int
  end

module Device_Language : DEVICE_LANGUAGE =
  struct
    type id = (string * string) (* (name * location)*)
    type actuator = string
    type typ = | Actuator | Sensor
    let compare i1 i2 =
      match i1, i2 with
      | (d1,l1),(d2,l2) ->
        match String.compare d1 d2 with
        | 0 -> String.compare l1 l2
        | _ -> String.compare d1 d2 

  end
