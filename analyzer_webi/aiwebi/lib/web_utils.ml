module type WEB =
  sig
    type url = string
    type permission = unit
    type hostname = string
    val string_of_url : url -> url
  end

module Web : WEB =
  struct
    type url = string
    type permission = unit
    type hostname = string
    let string_of_url (u : url) : url = u 
  end
