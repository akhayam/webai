(*-------------- Fix Point ------------------*)
val fix<a, b> (f :((a -> b) -> (a -> b))) (x : a) : b =
     let g = fix<a, b> f in
     f g x

(*------------- Print Utility ---------------*)

type out_chan
val std_out : out_chan
val flush_out : out_chan -> ()


(*------------ Constructed Types ------------*)

type voption<a> =
| VSome a
| VNone

type taint =
| Taint
| Untaint

(*------------ Data Structures --------------*)

(* lists *)

type _list<_>

val ladd<a> : (a,_list<a>) -> _list<a>
val ladd_end<a> : (_list<a>,a) -> _list<a>
val lappend<a> : (_list<a>,_list<a>) -> _list<a>
val lrev<a> : _list<a> -> _list<a>
val lhd<a> : _list<a> -> (a,_list<a>)
val lpick<a> : _list<a> -> (a,_list<a>)
val llength<a> : _list<a> -> nat
val lempty<a> : _list<a> -> _bool
val lmk_empty<a> : _list<a>
val lfold_l<a,b> : (a -> b -> a) -> a -> _list<b> -> a
val lmap<a,b> : (a -> b) -> _list<a> -> _list<b>
val lpartition<a> : (a -> _bool) -> _list<a> -> (_list<a>,_list<a>)

(* assoc lists *)
val lassoc<a,b> : a -> _list<(a,b)> -> (b,_list<(a,b)>)
val lassoc_opt<a,b> : a -> _list<(a,b)> -> voption<(b,_list<(a,b)>)>
val lremove_assoc<a,b> : a -> _list<(a,b)> -> ((a,b), _list<(a,b)>)

(* list of pairs *)
val lsplit<a,b> : _list<(a,b)> -> (_list<a>,_list<b>)
val lcombine<a,b> : _list<a> -> _list<b> -> _list<(a,b)>


(*--------------------- Value abstraction on CPO ------------------------*)

type aconst<a> =
  | Bot
  | Top
  | Aval a

(*----------------------- Data Types ---------------------------*)


(*--------------------- INTEGERS -------------------------*)
(*---------- Concrete Integers -------------*)

type int

val zero : int
val one : int
val minus_one : int

val add : (int, int) -> int
val sub : (int, int) -> int
val mul : (int, int) -> int


(*---------- Abstract Integers -------------*)

type a_int

type a_integer =
| Minf
| Pinf
| Int aint

val a_zero : a_integer
val a_one : a_integer
val a_minus_one : a_integer
val minf : a_integer = Minf
val pinf : a_integer = Pinf

(* operations on naturals *)

val a_add : (a_integer, a_integer) -> a_integer
val a_sub : (a_integer, a_integer) -> a_integer
val a_mul : (a_integer, a_integer) -> a_integer

type int_domain = aconst<a_integer>


(*--------------------- BOOLEANS -------------------------*)
(*---------- Concrete Booleans -------------*)

type _bool = | T | F


(*---------- Abstract Booleans -------------*)


     (* operations *)

val and (bs : (aconst<_bool>,aconst<_bool>)) : aconst<_bool> =
  match bs with
  | (Bot,_) -> Bot
  | (_,Bot) -> Bot
  | (Top,_) -> Top
  | (_,Top) -> Top
  | (Aval T, Aval T) -> Aval T
  | _ -> Aval F
  end
val orb (bs : (aconst<_bool>,aconst<_bool>)) : aconst<_bool> =
  match bs with
  | (Bot,Bot) -> Bot
  | (Top,Top) -> Top
  | (Bot,Top) -> Top
  | (Bot,Aval b) -> Aval b
  | (Aval b,Bot) -> Aval b
  | (Top,Aval b) -> match b with | T -> Aval T | F -> Top
  | (Aval b,Top) -> match b with | T -> Aval T | F -> Top
  | (Aval _, Aval T) -> Aval T
  | (Aval T, Aval _) -> Aval T
  | _ -> Aval F
  end

val not (b:aconst<_bool>) : aconst<_bool> =
  match b with
  | Bot -> Bot
  | Top -> Top
  | Aval T -> Aval F
  | Aval F -> Aval T
  end


(*--------- Abstract Values -------------------*)

(*Base values*)
type avalue =
| Nat aconst<nat>
| Bool aconst<_bool>
| Str str
| Unit
(*Extendend values*)
type avalue_ext<a> =
| Std avalue
| ExtValue a



(*--------- Abstract Expressions --------------*)

(*binary operations*)
type bop = | Add | Sub | Mul | LEq | GEq | Eq | AND | OR
(* unary operation *)
type uop = | Neg
(*Base Expressions*)
type aexpr<a,b> =
| Value b
| Var var
| BOp (bop,expr_ext<a,b>,expr_ext<a,b>)
| UOp (uop,expr_ext<a,b>)
(*Extendend Expressions*)
type aexpr_ext<a,b> =
| ExtExpr a
| Expr aexpr<a,b>


(**)








(* state monad*)

type state<_>
val init_state<a> : () -> state<a>

type st<a,b> := state<a> -> (b,state<a>)

val st_bind<a,b,c> (w: st<a,b>) (f : b -> st<a,c>) : st<a,c> =
  λ s : state<a> ->
  let (v',s') = w s in
  f v' s'

val st_ret<a,b> (v : b) : st<a,b> = λ s: state<a> -> (v,s)

type st_read_t<a> := var -> st<a,a>

val st_read<a> : st_read_t<a>


val st_m_read<a,b>  ((x,r_f) : (var,st_read_t<a>)) (f : a → st<a,b>): st<a,b> =
    let w = r_f x in
    st_bind<a,a,b> w (λ v : a -> f v)

type st_write_t<a> := var -> a -> st<a,()>

val st_write<a> : st_write_t<a>

val st_m_write<a,b,c>  ((x,v,w_f):(var,a,st_write_t<a>) ) (f: () → st<a,c>) : st<a,c> =
    let unit_state = w_f x v in
    st_bind<a,(),c> unit_state (λ _ : () -> f ())

