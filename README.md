# WEBAI

Static analysis of tWEBIoT.
To run :
 ```
    cd ./analyzer_webi/aiwebi/lib
    dune build
    dune exec main.exe
```

The program running is defined in the following folder:
```cd ./analyzer_webi/aiwebi/bin/main.ml```

